clear;
close all;
clc;

addpath ../functions_path
addpath ../util 
block = @(l,i) [1:l]+(l-1)*(i-1);
blockCat = @(l,i) [l*(i-1) + 1 : 1 : l*i];
blockDelta = @(c,delta) [c-delta:1:c+delta];
lVec = @(l,n) (1/l).^[n:-1:0];
lMat = @(l,n) diag( lVec(l,n) );

## POINTS

nSpline = 3;
nPts = nSpline * 5 + 1;

err_variance = 0.5;
power = 3;
pts = [linspace(1,11,nPts).^power+normrnd(0,err_variance,[1,nPts]);normrnd(5,2,[1, nPts])];

## 

pts = pts;
coef = zeros( 2 * nSpline, 6 );
coef_corr = coef;

## correction + Splines

k = 101;
time = @(T) linspace(0,T,k);
s = linspace(0,1,k);

nSample = nSpline * (k-1) + 1;

x = zeros(1,nSample); y = x;
xd = x;               yd = x;
xdd = x;              ydd = x;

xc = zeros(1,nSample); yc = xc;
xdc = xc;               ydc = xc;
xddc = xc;              yddc = xc;

V = x; Vr = x;
arcL = zeros(1,nSpline);
T = arcL;

sr_dot_extremis = zeros(1, 2 * nSpline);

for i = 1:nSpline
  if i < nSpline
    splineCorrection_index = blockDelta( 5 * i + 1 ,1);
    pts(:, splineCorrection_index ) = vMeanCorr( pts(:, splineCorrection_index) );
    
    splineCorrection_index = blockDelta( 5 * i + 1 ,2);
    pts(:, splineCorrection_index ) = aMeanCorr( pts(:, splineCorrection_index) );
  endif
  
  row_index = blockCat( 2, i );
  col_index = block( 6, i );
  coef(row_index,:) = bezier56( pts(:, col_index ) );
  
  ## coefficient extraction
  x_coef = coef(row_index(1),:);
  xd_coef = polyder(x_coef);
  xdd_coef = polyder(xd_coef);
  
  y_coef = coef(row_index(2),:);
  yd_coef = polyder(y_coef);
  ydd_coef = polyder(yd_coef);
  
  ## polynomial evaluation
  
  value_index = block(k,i);
  
  x(value_index) = polyval(x_coef,s);
  xd(value_index) = polyval(xd_coef,s);
  xdd(value_index) = polyval(xdd_coef,s);
  
  y(value_index) = polyval(y_coef,s);
  yd(value_index) = polyval(yd_coef,s);
  ydd(value_index) = polyval(ydd_coef,s);
  
  V(value_index) = sqrt( xd(value_index).^2 + yd(value_index).^2);
  if (i ~= nSpline) 
    arcL(i) = sum(V(value_index(1:end-1))) * s(2);
    #disp(["sum from ", num2str( value_index(1) ), " to ", num2str( value_index(end-1) )]);
  else 
    arcL(nSpline) = sum(V(value_index(1:end))) * s(2);
    #disp(["sum from ", num2str( value_index(1) ), " to ", num2str( value_index(end) )]);
  endif
  
  coef_corr(row_index,:) = coef(row_index,:) * lMat(arcL(i),5);
  
  ## coefficient extraction
  x_coefc = coef_corr(row_index(1),:);
  xd_coefc = polyder(x_coefc);
  xdd_coefc = polyder(xd_coefc);
  
  y_coefc = coef_corr(row_index(2),:);
  yd_coefc = polyder(y_coefc);
  ydd_coefc = polyder(yd_coefc);
  
  ## polynomial evaluation
  
  sr = s*arcL(i);
  
  xc(value_index) = polyval(x_coefc,sr);
  xdc(value_index) = polyval(xd_coefc,sr);
  xddc(value_index) = polyval(xdd_coefc,sr);
  
  yc(value_index) = polyval(y_coefc,sr);
  ydc(value_index) = polyval(yd_coefc,sr);
  yddc(value_index) = polyval(ydd_coefc,sr);
  
  Vr(value_index) = sqrt( xdc(value_index).^2 + ydc(value_index).^2);
  
  if(i > 1)
    
    ext_old = sum( arcL( i-1:i ) ) / ( 2 * arcL(i) );
    ext_new = sum( arcL( i-1:i ) ) / ( 2 * arcL(i-1) );
    
    sr_dot_extremis( blockCat(2, i) - 1 ) = [ext_old, ext_new];
    
  endif
  
endfor

## CORE OF TIME LAW

sr_dot_extremis = reshape(sr_dot_extremis,2,[]).';

ext_diff = diff(sr_dot_extremis,1,2);
T = ( 2 * arcL.' )./ ( ext_diff + 2 * sr_dot_extremis(:,1) );
sr_dot_coef = [ext_diff./T, sr_dot_extremis(:,1)];

t = zeros(1,nSample);
sr_dot = t;
for i = 1:nSpline
  sr_index = block(k,i);
  
  t( sr_index ) = linspace(0,T(i),k) + t(sr_index(1));
  sr_dot( sr_index ) = polyval( sr_dot_coef(i,:), t( sr_index ) - t(sr_index(1)));
  
endfor

## TODO: all simulation using x( sr( t )/l );

figure(1);
plotLine(pts,'-xr');
grid on;
hold on;
plot(x,y);

figure(2);
plot(V);
legend("V(s)");
grid on;

figure(3);
plot(t,Vr,'linewidth',2);
grid on;
hold on;
plot(t,sr_dot,'--r','linewidth',2);
legend("Re-normalized velocity", "Time law velocity profile");
xlabel("t [s]");
title("Time law profiling");

figure(4);
plot(t,Vr.*sr_dot,'linewidth',2);
grid on;
legend("V(t)");
xlabel("t [s]");
ylabel("V [m][s]^{-1}");
title("Final velocity profile");

s_dot = sr_dot;
for i = 1:3
  iSpline = block(k,i);
  s_dot( iSpline ) = s_dot( iSpline ) / arcL( i );
endfor
figure(5);
plot(t,s_dot);
grid on;
