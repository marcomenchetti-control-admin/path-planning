clear;
close all;
clc;

oldTrj = dlmread("../../tmp/trj.txt");
newTrj = dlmread("../../tmp/trjNew.txt");

s = 0:0.01:1;

x_old = polyval(oldTrj(1,:),s);
y_old = polyval(oldTrj(2,:),s);

[rows,~] = size(newTrj)

if rows == 2
  x_new = [polyval(newTrj(1,:),s)];
  y_new = [polyval(newTrj(2,:),s)];
elseif rows == 4
  x_new = [polyval(newTrj(1,:),s), polyval(newTrj(3,:),s)];
  y_new = [polyval(newTrj(2,:),s), polyval(newTrj(4,:),s)];
endif

plot(x_old,y_old);
hold on;
plot(x_new,y_new);