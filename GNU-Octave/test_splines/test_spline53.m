## (4,3) spline 4-th degree polynomial using groups of three points.
##
## Last spline is (3,2) if there is a single point left

clear;
close all;
clc;

addpath ../functions_path

## Undersampled sinusoid
##
## points = [0,  pi/2, pi, 3*pi/2, 2*pi;...
##           1,     0, -1,      0,    1;...
##           0, -pi/4,  0,   pi/4,    0];

## Well-sampled sinusoid
nSamples = 10;
points = [                 linspace( 0, 2*pi, nSamples );...
                    cos( linspace( 0, 2*pi, nSamples ) );...
           atan( -sin( linspace( 0, 2*pi, nSamples ) ) )];

[~,nPoints] = size(points);
even = false;
if mod(nPoints-3,2) == 1
  even = true;
endif
nSplines = (nPoints -1 + even)/2;

s = 0:0.001:1;
k = length(s);
coef = zeros(2*nSplines, 5);

x = zeros(1,k*nSplines); y = x;
xd = x; yd = x;
xdd = x; ydd = x;

# 'l' uneven
ptSelect = @(l,i) [1:l]+(l-1)*(i-1);
block = @(l,i) [l*(i-1) + 1 : 1 : l*i];

for i = 1:nSplines
  
  if (i == nSplines && even)
    inPt = points(:,end-1);
    fnPt = points(:,end);
    tmp = [[0;0], spline42(inPt,fnPt)];
  else
    pts = points(:,ptSelect(3,i));
    inPt = pts(:,1);
    mnPt = pts(:,2);
    fnPt = pts(:,3);
    
    tmp = spline53(inPt,mnPt,fnPt);
  endif
  
  coef(block(2,i),:) = tmp;
  
  x_coef = coef(2*(i-1) + 1,:);
  xd_coef = polyder(x_coef);
  xdd_coef = polyder(xd_coef);
  
  x(block(k,i)) = polyval( x_coef , s);
  xd(block(k,i)) = polyval( xd_coef , s);
  xdd(block(k,i)) = polyval( xdd_coef , s);
  
  y_coef = coef(2*i,:);
  yd_coef = polyder(y_coef);
  ydd_coef = polyder(yd_coef);
  
  y(block(k,i)) = polyval( y_coef , s);
  yd(block(k,i)) = polyval( yd_coef , s);
  ydd(block(k,i)) = polyval( ydd_coef , s);
endfor

plot(x,y)
hold on;

for i = 1:nSplines
  ## for every splines
  
  if (i==nSplines && even)
  ## last spline made of 2 points 
    tmp_pt = points(:,end+[-1:0]);
    for j = 1:2
      plot( tmp_pt(1,j), tmp_pt(2,j), '*r');
      plot( tmp_pt(1,j)+[0,cos(tmp_pt(3,j)) ], tmp_pt(2,j)+[0,sin(tmp_pt(3,j)) ],'-r');
    endfor
  else
  ## normal spline of three points
    pts = points(:,ptSelect(3,i));
    for j = 1:3
      plot( pts(1,j), pts(2,j), '*r');
     if(j!=2)
      plot( pts(1,j)+[0,cos(pts(3,j)) ], pts(2,j)+[0,sin(pts(3,j)) ],'-r');
      endif
    endfor
  endif
endfor

TH = atan2(yd,xd);
figure;
plot(TH);
hold on;
for i=1:nSplines
  
  plot([k*i, k*i],[TH(k*i)-1,TH(k*i)+1],'g');
  
endfor

V = sqrt(xd.^2 + yd.^2);
figure;
plot(V);
hold on;
for i=1:nSplines
  
  plot([k*i, k*i],[V(k*i)-1,V(k*i)+1],'g');
  
endfor

W = (ydd.*xd - yd.*xdd)./(V.^2);
figure;
plot(W);
hold on;
for i=1:nSplines
  
  plot([k*i, k*i],[W(k*i)-20,W(k*i)+20],'g');
  
endfor