clear;
close all;
clc;

pts = [linspace(0,10,6);linspace(5,10,6)];
coef = bezier56(pts);
s = 0:0.01:1;
x = polyval(coef(1,:),s);
y = polyval(coef(2,:),s);

plotLine(pts,'xr');
hold on;
plot(x,y);
grid on;