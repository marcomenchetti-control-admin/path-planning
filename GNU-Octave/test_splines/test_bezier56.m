clear;
close all;
clc;

addpath ../functions_path
addpath ../util

## Points
pts = [linspace(1,11,6)+normrnd(0,0.1,[1,6]);normrnd(5,5,[1, 6])]
plotLine(pts,"-xr")
coef = bezier56( pts );
s = 0:0.01:1;

x = polyval( coef(1,:), s );
y = polyval( coef(2,:), s );
hold on;
plot(x,y);
grid on;