## This script creates a 6-th degree polynomial connecting two different points
##
## Described as a (6,2) spline
clear;
close all;
clc;

addpath ../functions_path

## Undersampled sinusoid
##
## points = [0,  pi/2, pi, 3*pi/2, 2*pi;...
##           1,     0, -1,      0,    1;...
##           0, -pi/4,  0,   pi/4,    0];

## Well-sampled sinusoid
nSamples = 60;
points = [                 linspace( 0, 2*pi, nSamples );...
                    cos( linspace( 0, 2*pi, nSamples ) );...
           atan( -sin( linspace( 0, 2*pi, nSamples ) ) )];
          
v_in = 10; v_fn = 10;
w_in = 0;  w_fn = 0;

K = [1,1,1,1,1,1,1];

[~,j] = size(points);


coef = zeros(2*(j-1),6);
inPt = zeros(5,1);
fnPt = inPt;

s = 0:0.001:1;
k = length(s);

x = zeros(1, (j-1) * k);
y = x;

xd = x;
yd = x; 

xdd = x;
ydd = x;

block = @(l,i) [l*(i-1) + 1 : 1 : l*i];

for i = 1:j-1
  inPt = [points(:,i); 0.1; 0];
  fnPt = [points(:,i+1);0.1;0];
  if i==1
    inPt(4:5) = [v_in, w_in];
  elseif i == j-1
    fnPt(4:5) = [v_fn,w_fn];  
  endif
  tmp = spline62(inPt,fnPt)
  coef(block(2,i),:) = tmp;
  
  x_coef = coef(2*(i-1) + 1,:);
  xd_coef = polyder(x_coef);
  xdd_coef = polyder(xd_coef);
  
  x(block(k,i)) = polyval( x_coef , s);
  xd(block(k,i)) = polyval( xd_coef , s);
  xdd(block(k,i)) = polyval( xdd_coef , s);
  
  y_coef = coef(2*i,:);
  yd_coef = polyder(y_coef);
  ydd_coef = polyder(yd_coef);
  
  y(block(k,i)) = polyval( y_coef , s);
  yd(block(k,i)) = polyval( yd_coef , s);
  ydd(block(k,i)) = polyval( ydd_coef , s);
  
endfor

plot(x,y)

hold on;
for i=1:j
  xp = points(1,i);
  yp = points(2,i);
  th_p = points(3,i);
  plot(xp,yp,'*r')
  plot([xp,xp+cos(th_p)],[yp,yp+sin(th_p)],'r')
endfor

V = sqrt(xd.^2 + yd.^2);
figure;
plot(V);
hold on;
for i=1:j-1
  
  plot([k*i, k*i],[V(k*i)-1,V(k*i)+1],'g');
  
endfor

W = (ydd.*xd - yd.*xdd)./(V.^2);
figure;
plot(W);
hold on;
for i=1:j-1
  
  plot([k*i, k*i],[W(k*i)-20,W(k*i)+20],'g');
  
endfor
