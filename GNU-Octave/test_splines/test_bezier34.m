clear;
close all;
clc;

addpath ../functions_path
addpath ../util

## Points
pts = [linspace(1,11,4)+normrnd(0,0.1,[1,4]);normrnd(5,5,[1, 4])]

coef = bezier34( pts );
s = 0:0.01:1;

x = polyval( coef(1,:), s );
y = polyval( coef(2,:), s );

plot(x,y);
hold on;
for i=1:4
  plotLine( pts, '-xr' );
endfor
grid on;