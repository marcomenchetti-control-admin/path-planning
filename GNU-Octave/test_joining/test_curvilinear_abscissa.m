test_shared_input;
close all;

figure(1);
plot(t56,x56);
grid on;
title("x");

figure(2);
plot(t56,y56);
grid on;
title("y");

figure(3);
plot(x56,y56);
grid on;
hold on;
plotLine(pts56,'-xr');
title("Spline")

figure(4);
plot(t56,V56);
grid on;
title("V");

arclng = cumsum(V56)*s(2);
figure(5);
plot(t56,arclng);
hold on;
grid on;
title("arclength");

## Linear approximation of the arclength (from real values)
approx_arclng_LIN = linspace( arclng(1), arclng(end), length(arclng) );
figure(5);
plot(t56,approx_arclng_LIN);
err_arclng_LIN = arclng - approx_arclng_LIN;
figure(6);
plot( t56, err_arclng_LIN);
grid on;
title("Error Lin");

err_arclng_LIN_prcnt = err_arclng_LIN./arclng;
figure(7);
plot(t56, err_arclng_LIN_prcnt*100);
grid on;
title("Error Lin %")

## Segment approximation of the arclength (from the corrected points)

tmp_arclng_SEG = zeros(1,nPts-1);
for i = 1 : nPts-1
  tmp_arclng_SEG(i) = norm(pts56(:,i+1)-pts56(:,i));
endfor

tmp_arclngTOT_SEG = cumsum(tmp_arclng_SEG)

approx_arclng_SEG = zeros(1,nSample56);
for i = 1 : nSpline56
  
  if i == 1 dn = 0;
  else dn = tmp_arclngTOT_SEG( 5*(i-1) );
  endif
  up = tmp_arclngTOT_SEG( 5*i );
  
  approx_arclng_SEG(block(k,i)) = linspace(dn,up,k);
endfor

figure(5);
plot(t56,approx_arclng_SEG);

err_arclng_SEG = arclng - approx_arclng_SEG;
figure(8);
plot( t56, err_arclng_SEG);
grid on;
title("Error Seg");

err_arclng_SEG_prcnt = err_arclng_SEG./arclng;
figure(9);
plot(t56, err_arclng_SEG_prcnt*100);
grid on;
title("Error Seg %")

## Polynomial approximation of the arclength (from the corrected points)

# WORTHY?

## final plot

figure(5);
legend("real", "lin approx", "seg approx");
