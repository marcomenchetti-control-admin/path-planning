clear;
close all;
clc;

addpath ../functions_path
addpath ../util 
block = @(l,i) [1:l]+(l-1)*(i-1);
blockCat = @(l,i) [l*(i-1) + 1 : 1 : l*i];
blockDelta = @(c,delta) [c-delta:1:c+delta];

## POINTS

nMulti = 2;
nSpline56 = 3*nMulti;
nSpline34 = 5*nMulti;
nPts = nSpline56 * 5 + 1; ## Or either nSpline34 * 3 + 1;
th = linspace(0,pi,nPts);

err_variance = 0.01
pts = [ cos(th);sin(th) ] + normrnd(0,err_variance,[2,nPts]);

## correction + Splines

s = 0:0.01:1;
k = length(s);

## 34

pts34 = pts;
coef34 = zeros( 2 * nSpline34, 4 );
nSample34 = nSpline34 * (k-1) + 1;

x34 = zeros(1,nSample34); y34 = x34;
xd34 = x34;               yd34 = x34;
xdd34 = x34;              ydd34 = x34;

for i34 = 1:nSpline34
  if i34 < nSpline34
    splineCorrection_index34 = blockDelta( 3 * i34 + 1 ,1)
    pts34(:, splineCorrection_index34 ) = vMeanCorr( pts34(:, splineCorrection_index34) );
  endif
  
  row_index34 = blockCat( 2, i34 );
  col_index34 = block( 4, i34 );
  coef34(row_index34,:) = bezier34( pts34(:, col_index34 ) );
  
  ## coefficient extraction
  x_coef34 = coef34(row_index34(1),:);
  xd_coef34 = polyder(x_coef34);
  xdd_coef34 = polyder(xd_coef34);
  
  y_coef34 = coef34(row_index34(2),:);
  yd_coef34 = polyder(y_coef34);
  ydd_coef34 = polyder(yd_coef34);
  
  ## polynomial evaluation
  
  value_index34 = block(k,i34);
  
  x34(value_index34) = polyval(x_coef34,s);
  xd34(value_index34) = polyval(xd_coef34,s);
  xdd34(value_index34) = polyval(xdd_coef34,s);
  
  y34(value_index34) = polyval(y_coef34,s);
  yd34(value_index34) = polyval(yd_coef34,s);
  ydd34(value_index34) = polyval(ydd_coef34,s);
  
endfor

## 56

pts56 = pts;
coef56 = zeros( 2 * nSpline56, 6 );
nSample56 = nSpline56 * (k-1) + 1;

x56 = zeros(1,nSample56); y56 = x56;
xd56 = x56;               yd56 = x56;
xdd56 = x56;              ydd56 = x56;

for i56 = 1:nSpline56
  if i56 < nSpline56
    splineCorrection_index56 = blockDelta( 5 * i56 + 1 ,1)
    pts56(:, splineCorrection_index56 ) = vMeanCorr( pts56(:, splineCorrection_index56) );
    
    splineCorrection_index56 = blockDelta( 5 * i56 + 1 ,2)
    pts56(:, splineCorrection_index56 ) = aMeanCorr( pts56(:, splineCorrection_index56) );
  endif
  
  row_index56 = blockCat( 2, i56 );
  col_index56 = block( 6, i56 );
  coef56(row_index56,:) = bezier56( pts56(:, col_index56 ) );
  
  ## coefficient extraction
  x_coef56 = coef56(row_index56(1),:);
  xd_coef56 = polyder(x_coef56);
  xdd_coef56 = polyder(xd_coef56);
  
  y_coef56 = coef56(row_index56(2),:);
  yd_coef56 = polyder(y_coef56);
  ydd_coef56 = polyder(yd_coef56);
  
  ## polynomial evaluation
  
  value_index56 = block(k,i56);
  
  x56(value_index56) = polyval(x_coef56,s);
  xd56(value_index56) = polyval(xd_coef56,s);
  xdd56(value_index56) = polyval(xdd_coef56,s);
  
  y56(value_index56) = polyval(y_coef56,s);
  yd56(value_index56) = polyval(yd_coef56,s);
  ydd56(value_index56) = polyval(ydd_coef56,s);
  
endfor

figure(1);
plotLine(pts,'-xg');
hold on;
grid on;
plotLine(pts34,'-xb');
plotLine(pts56,'-xr');
legend("Pts","Pts34","Pts56");

figure(2);
plotLine(pts,'-xg');
hold on;
grid on;
plotLine(pts34,'-xb');
plot(x34,y34,'-b');

figure(3);
plotLine(pts,'-xg');
hold on;
grid on;
plotLine(pts56,'-xr');
plot(x56,y56,'-r');

figure(4);
plot(x34,y34,'-b');
hold on;
grid on;
plot(x56,y56,'-r');

## variables34
t34 = linspace(0,100,nSample34);
TH34 = atan2(yd34,xd34);
V34 = sqrt(xd34.^2 + yd34.^2);
W34 = (ydd34.*xd34 - yd34.*xdd34)./(V34.^2);
Rc34 = V34 ./ W34;

## variables56
t56 = linspace(0,100,nSample56);
TH56 = atan2(yd56,xd56);
V56 = sqrt(xd56.^2 + yd56.^2);
W56 = (ydd56.*xd56 - yd56.*xdd56)./(V56.^2);
Rc56 = V56 ./ W56;

VARS = {t34,TH34,V34,W34,1./Rc34;t56,TH56,V56,W56,1./Rc56};
titles = {'TH','V','W','1/Rc'};

for col = 1:4
  
  figure(4+col);
  plot(VARS{1,1},VARS{1,col+1});
  hold on;
  grid on;
  plot(VARS{2,1},VARS{2,col+1});
  legend("34","56");
  title( titles{col} );
  
   if col == 1
    plot([0,100],[pi, pi],'k');
    plot([0,100],[-pi, -pi],'k');
  endif

endfor
