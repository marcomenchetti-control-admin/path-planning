clear;
close all;
clc;

addpath ../functions_path
addpath ../util 
block = @(l,i) [1:l]+(l-1)*(i-1);
blockCat = @(l,i) [l*(i-1) + 1 : 1 : l*i];
colors = {'r','b','g'};
## POINTS

pts = [linspace(1,11,11)+normrnd(0,0.1,[1,11]);normrnd(5,2,[1, 11])]
         
figure(1);
plotLine(pts,'-xr');

## Interest points: VELOCITY PHASE

p_center = pts(:,6);
p_dn = pts(:,5);
p_up = pts(:,7);
p_mean = (p_up - p_dn)*5/2;
p_mean_dir = p_mean/norm(p_mean);

## POINTS MEAN CORRECTION

pts_mean = [ pts(:,1:4), vMeanCorr( pts(:,5:7) ), pts(:,8:end) ]

## POINTS DIRECTION CORRECTION

p_dn_snd = p_center + ( (p_dn - p_center).' * p_mean_dir ) * p_mean_dir;
p_up_snd = p_center + ( (p_up - p_center).' * p_mean_dir ) * p_mean_dir;

pts_dir = [ pts(:,1:4), p_dn_snd, p_center, p_up_snd, pts(:,8:end) ]

## Interest points: ACCELERATION PHASE
p_extL = pts(:,4); p_extR = pts(:,8);
p_extM = ( p_extL + p_extR ) / 2;

## ACCELERATION for mean correction

pts_mean_acc = [pts_mean(:,1:3), aMeanCorr( pts_mean(:,4:8) ), pts_mean(:,9:end)]

figure(2);
plotLine(pts_mean_acc,'-xb');

## ACCELERATION for direction correction
p_midL_dir = pts_dir(:,5); p_midR_dir = pts_dir(:,7);
p_extL_dir = p_extM + p_midL_dir - p_midR_dir;
p_extR_dir = p_extM - p_midL_dir + p_midR_dir;

pts_dir_acc = [pts_dir(:,1:3), p_extL_dir, pts_dir(:,5:7), p_extR_dir, pts_dir(:,9:end)]

figure(3);
plotLine(pts_dir_acc,'-xg');

## SPLINES

s = 0:0.001:1;
k = length(s);
nSplines = 2; ## TODO: General
[~,nPts] = size(pts);
nSamples = nSplines*(k-1) + 1;

x = zeros(3,nSamples);  y = x;
xd = x;                 yd = x;
xdd = x;                ydd = x;

coef = zeros(2*nSplines, 4*3);

pts_full = [pts, pts_mean_acc, pts_dir_acc];

for type = 1:3
  
  pts_type_extract = pts_full(:, blockCat(nPts,type) );
  coef_COL_index = blockCat(6,type);
  
  for actualSpline = 1:nSplines
    
    coef_ROW_index = blockCat(2,actualSpline);
    spline_index = block(6,actualSpline);
    
    ## spline computation
    spline_pts = pts_type_extract(:, spline_index );
    coef(coef_ROW_index,coef_COL_index) = bezier56( spline_pts );
    
    ## coefficient extraction
    x_coef = coef( coef_ROW_index(1), coef_COL_index );
    xd_coef = polyder(x_coef);
    xdd_coef = polyder(xd_coef);
    
    y_coef = coef( coef_ROW_index(2), coef_COL_index );
    yd_coef = polyder(y_coef);
    ydd_coef = polyder(yd_coef);
    
    ## Polynomial evaluation
    valued_index = block(k, actualSpline);
    
    x(type,valued_index) = polyval(x_coef,s);
    xd(type,valued_index) = polyval(xd_coef,s);
    xdd(type,valued_index) = polyval(xdd_coef,s);
    
    y(type,valued_index) = polyval(y_coef,s);
    yd(type,valued_index) = polyval(yd_coef,s);
    ydd(type,valued_index) = polyval(ydd_coef,s);
    
  endfor
  
  figure(type);
  hold on;
  grid on;
  plot(x(type,:), y(type,:), colors{type});
  
endfor

## KINEMATIC VARIABLES

TH = atan2(yd,xd);
V = sqrt(xd.^2 + yd.^2);
W = (ydd.*xd - yd.*xdd)./(V.^2);
Rc = V ./ W;

VARS = [TH;V;W;1./Rc];
titles = {'TH','V','W','1/Rc'};

for figures = 4:7
  
  plotting_var = VARS( blockCat(3,figures-3) ,:);
  figure(figures);
  plot( plotting_var(1,:), colors{1} );
  title( titles{figures-3} );
  grid on;
  hold on;
  
  if figures == 4
    plot([1,nSamples],[pi, pi],'k');
    plot([1,nSamples],[-pi, -pi],'k');
  endif
  
  for type = 2:3
    
    plot( plotting_var(type,:), colors{type} );
    
  endfor
  
endfor

## plot of superposed points

figure(8)
plotLine(pts,'-*r');
hold on;
grid on;
plotLine(pts_mean_acc,'-*b');
plotLine(pts_dir_acc,'-*g');

## plot of the derivatives of the splines

tmp_xDerivatives = [x;xd;xdd];
tmp_yDerivatives = [y;yd;ydd];

for i = 1:3
  figure_index = blockCat(2,4+i);
  
  extract_xDerivative = tmp_xDerivatives( blockCat(3,i),: );
  figure( figure_index(1) );
  plot( extract_xDerivative(1,:), colors{1} );
  hold on; grid on; title(["X derivative of order ", num2str(i-1)]);
  
  extract_yDerivative = tmp_yDerivatives( blockCat(3,i),: );
  figure( figure_index(2) );
  plot( extract_yDerivative(1,:), colors{1} );
  hold on; grid on; title(["Y derivative of order ", num2str(i-1)]);
  
  for j = 2:3
    figure( figure_index(1) );
    plot( extract_xDerivative(j,:), colors{j} );
    
    figure( figure_index(2) );
    plot( extract_yDerivative(j,:), colors{j} );
  endfor
  
endfor
