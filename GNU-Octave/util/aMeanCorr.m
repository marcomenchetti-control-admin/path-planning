function out = aMeanCorr(pts)
## TODO: should accept only 5 points

  ## Interest points: ACCELERATION PHASE
  p_extL = pts(:,1); p_extR = pts(:,end);
  p_extM = ( p_extL + p_extR ) / 2;
  
  ## ACCELERATION for mean correction
  p_midL_mean = pts(:,2); p_midR_mean = pts(:,4);
  
  p_extL_mean = p_extM + p_midL_mean - p_midR_mean;
  p_extR_mean = p_extM - p_midL_mean + p_midR_mean;
  
  out = [p_extL_mean, pts(:,2:4), p_extR_mean];
endfunction