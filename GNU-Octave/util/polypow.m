function out = polypow(coef, deg)
  
  if deg <= 0
    out = 1;
  elseif deg == 1
    out = coef;
  else
    out = conv(coef, polypow(coef, deg-1));
  endif
  
endfunction