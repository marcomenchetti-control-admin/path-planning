function out = vMeanCorr(pts)
## TODO: should accept only 3 points
  ## Interest points

  p_center = pts(:,2);
  p_dn = pts(:,1);
  p_up = pts(:,3);

  p_mean = (p_up - p_dn)/2;

  ## POINTS MEAN CORRECTION

  p_dn_prime = p_center - p_mean;
  p_up_prime = p_center + p_mean;

  out = [p_dn_prime, p_center, p_up_prime];
  
endfunction