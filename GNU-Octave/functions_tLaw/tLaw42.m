function out = tLaw42(p0,p1,r0,r1)
## generates the coefficients for the time law:
##  - from p0 to p1
##  - with initial velocity r0 and final velocity r1
  
  M = [ 2  1 -2  1;
       -3 -2  3 -1;
        0  1  0  0;
        1  0  0  0];
  
  out = ( M * [p0; r0; p1; r1] ).';
  
endfunction