function out = spline53(inPt,mnPt,fnPt)

  ti = inPt(3); tf = fnPt(3);
  
  xi = inPt(1); xm = mnPt(1); xf = fnPt(1);
  xi_d = cos(ti); xf_d = cos(tf);
  
  yi = inPt(2); ym = mnPt(2); yf = fnPt(2);
  yi_d = sin(ti); yf_d = sin(tf);
  
  K =  [  -8   16   -8   -2    2;
          18  -32   14    5   -3;
         -11   16   -5   -4    1;
          -0    0    0    1    0;
           1    0    0    0    0];

  x_coef = ( K * [xi; xm; xf; xi_d; xf_d] ).';
  y_coef = ( K * [yi; ym; yf; yi_d; yf_d] ).';
  
  out = [x_coef;y_coef];
  
endfunction
