function out = bezier34( pts ) 
  
  pt1 = pts(:,1); 
  pt2 = pts(:,2);
  pt3 = pts(:,3);
  pt4 = pts(:,4);
  
  M = [-1  3 -3  1;
        3 -6  3  0;
       -3  3  0  0;
        1  0  0  0];
        
  pts = [pt1, pt2, pt3, pt4].';
  
  x_coef = ( M * pts(:,1) ).';
  y_coef = ( M * pts(:,2) ).';
  
  out = [x_coef;y_coef];
  
endfunction