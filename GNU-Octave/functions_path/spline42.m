function out = spline42(inPt, fnPt)
  
  ti = inPt(3); tf = fnPt(3);
  
  xi = inPt(1); xf = fnPt(1);
  xi_d = cos(ti); xf_d = cos(tf);
  
  yi = inPt(2); yf = fnPt(2);
  yi_d = sin(ti); yf_d = sin(tf);
  
  K = [ 2  1 -2  1;
       -3 -2  3 -1;
        0  1  0  0;
        1  0  0  0];
        
  x_coef = ( K * [xi; xi_d; xf; xf_d] ).';
  y_coef = ( K * [yi; yi_d; yf; yf_d] ).';
  
  out = [x_coef;y_coef];
  
endfunction
