function out = bezier5( pts )
  
  pt1 = pts(:,1);
  pt2 = pts(:,2);
  pt3 = pts(:,3);
  pt4 = pts(:,4);
  pt5 = pts(:,5);
  pt6 = pts(:,6);
  
  M = [  -1   5 -10  10  -5   1;
          5 -20  30 -20   5   0;
        -10  30 -30  10   0   0;
         10 -20  10   0   0   0;
         -5   5   0   0   0   0;
          1   0   0   0   0   0];
        
  pts = [pt1, pt2, pt3, pt4, pt5, pt6].';
  
  x_coef = ( M * pts(:,1) ).';
  y_coef = ( M * pts(:,2) ).';
  
  out = [x_coef;y_coef];
  
endfunction