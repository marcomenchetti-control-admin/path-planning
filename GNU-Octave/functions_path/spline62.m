## Copyright (C) 2021 Marco Menchetti
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} path (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Marco Menchetti <marco@marco-HP-Pavilion>
## Created: 2021-08-02

function out = path (initPt, finalPt)
  ## initPt  = [x_in, y_in, th_in, v_in, w_in];
  ## finalPt = [x_fn, y_fn, th_fn, v_fn, w_fn];
  x_in = initPt(1); x_fn = finalPt(1);
  y_in = initPt(2); y_fn = finalPt(2); 
  th_in = initPt(3); th_fn = finalPt(3);
  v_in = initPt(4); v_fn = finalPt(4);
  w_in = initPt(5); w_fn = finalPt(5);
  
  ## Conditioning for acceleration values
  if(abs(cos(th_fn)) > abs(sin(th_fn)))
    ay_fn = w_fn*v_fn/cos(th_fn);
    ax_fn = 0;
  else
    ay_fn = 0;
    ax_fn = -w_fn*v_fn/sin(th_fn);;
  end

  if(abs(cos(th_in)) > abs(sin(th_in)))
    ay_in = w_in*v_in/cos(th_in);
    ax_in = 0;
  else
    ay_in = 0;
    ax_in = -w_in*v_in/sin(th_in);;
  end
  
  ## Build 2 polynomials of 5-th degree
  K = [ -6   6 -3 -3 -0.5 0.5;
        15 -15  8  7  1.5  -1;
       -10  10 -6 -4 -1.5 0.5;
         0   0  0  0  0.5   0;
         0   0  1  0    0   0;
         1   0  0  0    0   0];
  x_poly = (K*[x_in; x_fn; v_in*cos(th_in); v_fn*cos(th_fn); ax_in; ax_fn]).';
  y_poly = (K*[y_in; y_fn; v_in*sin(th_in); v_fn*sin(th_fn); ay_in; ay_fn]).';
  
  out = [x_poly; y_poly];
endfunction
