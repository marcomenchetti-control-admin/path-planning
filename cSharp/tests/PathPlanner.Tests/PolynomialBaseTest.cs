using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdObjects
{
    public class PolynomialBaseTest
    {
        [Fact]
        public void SumTestA()
        {
        //Given
            List<double> a = new List<double>() {1, 2, 3, 4, 5};
            List<double> b = new List<double>() {3, 4, 5};
            List<double> sum = new List<double>() {1, 2, 6, 8, 10};
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial bPoly = new Polynomial( b );
            Polynomial sumPoly = new Polynomial( sum );
        
        //Then
            Assert.Equal( sumPoly, aPoly + bPoly );
        }

        [Fact]
        public void SumTestB()
        {
        //Given
            List<double> a = new List<double>() {1, 2, 3, 4, 5};
            List<double> b = new List<double>() {3, 4, 5};
            List<double> sum = new List<double>() {1, 2, 6, 8, 10};
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial bPoly = new Polynomial( b );
            Polynomial sumPoly = new Polynomial( sum );
        
        //Then
            Assert.Equal( sumPoly, bPoly + aPoly);
        }

        [Fact]
        public void SubTestA()
        {
        //Given
            List<double> a = new List<double>() {1, 2, 3, 4, 5};
            List<double> b = new List<double>() {3, 4, 5};
            List<double> sub = new List<double>() {1, 2, 0, 0, 0};
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial bPoly = new Polynomial( b );
            Polynomial subPoly = new Polynomial( sub );
        
        //Then
            Assert.Equal( subPoly, aPoly - bPoly );
        }

        [Fact]
        public void SubInvTest()
        {
        //Given
            List<double> a = new List<double>() {1, 2, 3, 4, 5};
            List<double> aMinus = new List<double>() {-1, -2, -3, -4, -5};
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial aMinusPoly = new Polynomial( aMinus );
        
        //Then
            Assert.Equal( aMinusPoly, -aPoly);
        }

        [Fact]
        public void ConvTestAGreaterThanB()
        {
        //Given
            List<double> a = new List<double>() {1, 1, 1}; // x^2 + x + 1
            List<double> b = new List<double>() {1, 0}; // x
            List<double> conv = new List<double>() {1, 1, 1, 0}; // x^3 + x^2 + x
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial bPoly = new Polynomial( b );
            Polynomial convPoly = new Polynomial( conv );
        
        //Then
            Assert.Equal( convPoly, aPoly * bPoly );
        }

        [Fact]
        public void ConvTestBGreaterThanA()
        {
        //Given
            List<double> a = new List<double>() {1, 1, 1}; // x^2 + x + 1
            List<double> b = new List<double>() {1, 0}; // x
            List<double> conv = new List<double>() {1, 1, 1, 0}; // x^3 + x^2 + x
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial bPoly = new Polynomial( b );
            Polynomial convPoly = new Polynomial( conv );
        
        //Then
            Assert.Equal( convPoly, bPoly * aPoly );
        }

        [Fact]
        public void PowTest()
        {
        //Given
            List<double> a = new List<double>() {1, 2}; // x + 2
            List<double> aCube = new List<double>() {1,6,12,8}; // x^3 + 3x^2 + 12x + 8 
        
        //When
            Polynomial aPoly = new Polynomial( a );
            Polynomial aCubePoly = new Polynomial( aCube );
        
        //Then
            Assert.Equal( aCubePoly, aPoly.Pow(3) );
        }

        [Fact]
        public void DerivateTest()
        {
        //Given
            List<double> parabolic = new List<double>() {1, 3, 0}; // x^2 + 3x + 0;
            List<double> expectedDerivative = new List<double>() {2, 3}; // 2x + 3;
            
        //When
            Polynomial paraPoly = new Polynomial( parabolic );
        //Then
            Assert.Equal( new Polynomial(expectedDerivative), paraPoly.Derivate() );
        }

        [Fact]
        public void IntegrateTest()
        {
        //Given
            List<double> rect = new List<double>() {2, 3}; // 2x + 3;
            List<double> expectedIntegral = new List<double>() {1, 3, 0}; // x^2 + 3x + 0;
        //When
            Polynomial rectPoly = new Polynomial( rect );
            Polynomial expePoly = new Polynomial( expectedIntegral);
        //Then
            Assert.Equal( expePoly, rectPoly.Integrate(0) );
        }

        [Fact]
        public void ToStringTest()
        {
        //Given
            string referenceString = "[ 1 2 3 4 ]";
            var referenceCoefficients = new List<double>() {1, 2, 3, 4};
        //When
            Polynomial referencePolynomial = new Polynomial( referenceCoefficients );

        //Then
            Assert.Equal( referenceString, referencePolynomial.ToString() );
        }

        [Theory]
        [InlineData(0,0)]
        [InlineData(1,1)]
        [InlineData(2,4)]
        [InlineData(3,9)]
        [InlineData(4,16)]
        public void EvalTest(double x, double xSquared)
        {
        //Given
            List<double> parabCoef = new List<double>(){1,0,0};
        //When
            Polynomial parabola = new Polynomial( parabCoef );
        //Then
            Assert.Equal( xSquared, parabola.Eval(x) );
        }
    }
}
