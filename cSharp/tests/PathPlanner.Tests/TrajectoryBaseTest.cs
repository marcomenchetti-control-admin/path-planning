using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdSplines;
using Ugv.PathPlanner.stdTrajectories;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdTrajectories
{
    public class TrajectoryBaseTest
    {

        [Fact]
        public void TwoPointStream()
        {
        //Given
            List<Point2> doublePointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(2.5,5)
            };
            List<Point2> expectedPointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(0.5,1),
                new Point2(1,2),
                new Point2(1.5,3),
                new Point2(2,4),
                new Point2(2.5,5)
            };
        //When
            var trj = new Trajectory( doublePointList );
            trj.PointStream.ForEach( Console.WriteLine );
            
         // Then
            bool outEquality = true;
            for( int i = 0; i < expectedPointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == expectedPointList[i];
            }
            Assert.True(outEquality);
        }
        [Fact]
        public void CompleteStreamTest()
        {
        // Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(7,14)
            };
            List<Point2> expectedPointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(6.25,12.5),
                new Point2(6.5,13),
                new Point2(6.75,13.5),
                new Point2(7,14)
            };
        // When
            Trajectory trj = new Trajectory( pointList );
            
        // Then
            bool outEquality = true;
            for( int i = 0; i < expectedPointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == expectedPointList[i];
            }
            Assert.True(outEquality);
        }
        [Fact]
        public void InterpolateFromStreamTest()
        {
        // Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(7,14)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(2.625,5.25),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(6.625,13.25),
                new Point2(6.5,13),
                new Point2(6.75,13.5),
                new Point2(7,14)
            };
        // When
            Trajectory trj = new Trajectory( pointList );
            BezierSpline5 spline1 = new BezierSpline5( expectedPointList.GetRange(0,6) );
            BezierSpline5 spline2 = new BezierSpline5( expectedPointList.GetRange(5,6) );
        //Then
            bool firstEqual = spline1 == trj.SplineStream[0];
            bool secondEqual = spline2 == trj.SplineStream[1];
            Assert.True( firstEqual && secondEqual );
        }
        [Fact]
        public void AddRangeTest()
        {
        // Given
            List<Point2> pointListA = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12)
            };
            List<Point2> pointListB = new List<Point2>()
            {
                new Point2(7,14)
            };
            List<Point2> expectedPointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(6.25,12.5),
                new Point2(6.5,13),
                new Point2(6.75,13.5),
                new Point2(7,14)
            };
        // When
            Trajectory trj = new Trajectory( pointListA );
            trj.AddRange(pointListB);
            
        // Then
            bool outEquality = true;
            for( int i = 0; i < expectedPointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == expectedPointList[i];
            }
            Assert.True(outEquality);
        }
        [Fact]
        public void AddTest()
        {
        // Given
            List<Point2> pointListA = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12)
            };
            Point2 newPoint = new Point2(7,14);
            List<Point2> expectedPointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10),
                new Point2(6,12),
                new Point2(6.25,12.5),
                new Point2(6.5,13),
                new Point2(6.75,13.5),
                new Point2(7,14)
            };
        // When
            Trajectory trj = new Trajectory( pointListA );
            trj.Add(newPoint);
            
        // Then
            bool outEquality = true;
            for( int i = 0; i < expectedPointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == expectedPointList[i];
            }
            Assert.True(outEquality);
        }
        [Fact]
        public void CorrectStreamTest()
        {
        // Given
            List<Point2> pointListA = new List<Point2>() // Pseudo-random numbers
            {
                new Point2(0,0), 
                new Point2(2,4),
                new Point2(4,8),
                new Point2(6,12),
                new Point2(8,16),
                new Point2(10,20),
                new Point2(1,2),
                new Point2(3,6),
                new Point2(5,10),
                new Point2(7,14),
                new Point2(9,18)
            };
            List<Point2> pointListExpected = new List<Point2>() // Octave verified
            {
                new Point2(    0,  0),
                new Point2(    2,  4),
                new Point2(    4,  8),
                new Point2( 11.5, 23),
                new Point2( 13.5, 27),
                new Point2(   10, 20),
                new Point2(  6.5, 13),
                new Point2( -2.5, -5),
                new Point2(    5, 10),
                new Point2(    7, 14),
                new Point2(    9, 18)
            };

            Trajectory trj = new Trajectory( pointListA );
        // Then
            bool outEqual = true;
            for(int i = 0; i < pointListExpected.Count; ++i)
            {
                outEqual &= trj.PointCorrection[i] == pointListExpected[i];
            }
            Assert.True(outEqual);
        }
    }
}
