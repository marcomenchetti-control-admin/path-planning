using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdObjects
{
    public class Point2BaseTest
    {
        [Fact]
        public void SumTest()
        {
        //Given
        Point2 pointA = new Point2(1,1);
        //When
        
        //Then
        Assert.Equal(new Point2(2,2), pointA + pointA);
        }

        [Fact]
        public void SubTest()
        {
        //Given
        Point2 pointA = new Point2(1,1);
        //When
        
        //Then
        Assert.Equal(new Point2(0,0), pointA - pointA);
        }

        [Fact]
        public void ProdTestA()
        {
        //Given
        Point2 pointA = new Point2(1,1);
        //When
        
        //Then
        Assert.Equal(new Point2(2,2), 2*pointA);
        }
        [Fact]
        public void ProdTestB()
        {
        //Given
        Point2 pointA = new Point2(1,1);
        //When
        
        //Then
        Assert.Equal(new Point2(2,2), pointA*2);
        }

        [Fact]
        public void DivTestA()
        {
        //Given
        Point2 pointA = new Point2(1,2);
        //When
        
        //Then
        Assert.Equal(new Point2(2,1), 2/pointA);
        }
        [Fact]
        public void DivTestB()
        {
        //Given
        Point2 pointA = new Point2(2,4);
        //When
        
        //Then
        Assert.Equal(new Point2(1,2), pointA/2);
        }

        [Fact]
        public void MinusTest()
        {
        //Given
        Point2 pointA = new Point2(1,1);
        //When
        
        //Then
        Assert.Equal(new Point2(-1,-1), -pointA);
        }
    }
}