using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdObjects
{
    public class PolynomialAdvancedTest
    {
        [Fact]
        public void ArrayConstructor()
        {
        //Given
            double[] arrayCoefs = new[]{1d,2,3,4};
            List<double> listCoefs = new List<double>( arrayCoefs );
        //When
            Polynomial arrayPoly = new Polynomial( arrayCoefs );
            Polynomial listPoly  = new Polynomial( listCoefs );
        //Then
            Assert.Equal( listPoly, arrayPoly );
        }
        [Fact]
        public void DotProductTest()
        {
        //Given
            double[] aCoefs = new[] {1d, 2, 3, 4};
            double[] bCoefs = new[] {2d, 2, 2, 2};
            double[] dotProdCoefs = new[] {2d, 4, 6, 8};
        
        //When
            Polynomial aPoly = new Polynomial( aCoefs );
            Polynomial bPoly = new Polynomial( bCoefs );
        //Then
            Assert.Equal( new Polynomial( dotProdCoefs ), aPoly.DotProduct( bPoly ) );
        }
        [Fact]
        public void ToArrayTest()
        {
        //Given
            double[] coefs = new[]{0d,1,2,3};
        //When
            Polynomial poly = new Polynomial( coefs );
        //Then
            Assert.Equal( coefs, poly.ToArray() );
        }
        [Fact]
        public void OperationCatenation()
        {
        //Given
            double[] coefs = new[] {1d, 2, 3};
            double[] expectedCoefs = new[] {4d, 8, 4};
        //When
            Polynomial poly = new Polynomial( coefs );
        //Then
            double[] derivateAndPower = poly.Derivate(1).Pow(2).ToArray();
            Assert.Equal( expectedCoefs, derivateAndPower);
        }

        [Fact]
        public void CompositionTest()
        {
        //Given
            double[] aCoefs = new double[] { 1d, 2, 1 };
            double[] bCoefs = new double[] { 1d, 1 };
            double[] cCoefs = new double[] { 1d, 4, 4 };
        //When
            Polynomial aPoly = new Polynomial( aCoefs );
            Polynomial bPoly = new Polynomial( bCoefs );
            Polynomial cPoly = new Polynomial( cCoefs );

            //Then
            Assert.Equal( cPoly, aPoly.Compose( bPoly ) );
        }
    }
}
