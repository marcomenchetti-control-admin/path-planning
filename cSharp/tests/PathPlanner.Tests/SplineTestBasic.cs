using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdSplines;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdSplines
{
    public class SplineTestBasic
    {
        [Fact]
        public void AlgorithmTest()
        {
        //Given
            Point2 firstPoint = new Point2(0,0);
            Point2 lastPoint = new Point2(5,10);
            List<Point2> points = new List<Point2>()
            {
                firstPoint,
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                lastPoint
            };
        // Octave tested coefficients
            double[] xCoefs = new double[]{ 0d,  0,  0,  0,  5,  0};
            double[] yCoefs = new double[]{ 0d,  0,  0,  0, 10,  0};
        //When
            BezierSpline5 spline = new BezierSpline5(points);
        //Then
            bool xEqual = spline.Polynomials[0] == new Polynomial(xCoefs);
            bool yEqual = spline.Polynomials[1] == new Polynomial(yCoefs);
            Assert.True( xEqual && yEqual );
        }
        [Fact]
        public void EvalTest()
        {
        //Given
            Point2 firstPoint = new Point2(0,0);
            Point2 lastPoint = new Point2(5,10);
            List<Point2> points = new List<Point2>()
            {
                firstPoint,
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                lastPoint
            };
        //When
            BezierSpline5 spline = new BezierSpline5(points);
        //Then
            bool inEqual = firstPoint == spline.Eval(0); // 0 is initial point
            bool fnEqual =  lastPoint == spline.Eval(1); // 1 is final point
            Assert.True( inEqual && fnEqual );
        }
        [Fact]
        public void ToStringTest()
        {
        //Given
            Point2 firstPoint = new Point2(0,0);
            Point2 lastPoint = new Point2(5,10);
            List<Point2> points = new List<Point2>()
            {
                firstPoint,
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                lastPoint
            };
        // Octave tested coefficients
            double[] xCoefs = new double[]{ 0d,  0,  0,  0,  5,  0};
            double[] yCoefs = new double[]{ 0d,  0,  0,  0, 10,  0};
        //When
            BezierSpline5 spline = new BezierSpline5( points );
            Polynomial xPoly = new Polynomial( xCoefs );
            Polynomial yPoly = new Polynomial( yCoefs );
        //Then
            string expectedString = "X: " + xPoly.ToString();
            expectedString += "\nY: " + yPoly.ToString();
            Assert.Equal( expectedString, spline.ToString() );
        }
        [Fact]
        public void TypeTest()
        {
        //Given
        
        //When
        
        //Then
        Assert.True( typeof( BezierSpline5 ).IsSubclassOf( typeof( BaseSpline ) ) );
        }
        [Fact]
        public void ArcLengthTest()
        {
        //Given
            List<Point2> pts = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,1),
                new Point2(2,2),
                new Point2(3,3),
                new Point2(4,4),
                new Point2(5,5)
            };
            double exptectedArclength = 5 * Math.Sqrt( 2 ); // Length of a straight line
            int nPercentage = 1;
            double precision = exptectedArclength * nPercentage / 100;
        //When
            BezierSpline5 spline = new BezierSpline5( pts );
            double arcLength = spline.ArcLength();
        //Then
            bool upperBounded = arcLength <= exptectedArclength + precision;
            bool lowerBounded = arcLength >= exptectedArclength - precision;
            
            Assert.True( upperBounded && lowerBounded );
        }
    }
}
