using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdTrajectories;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdTrajectories
{
    public class TimeLawTest
    {
        [Fact]
        public void ArcLengthTest()
        {
        //Given
            List<Point2> pointList = new List<Point2>();
            for( int i = 0; i < 16; ++i )
            {   
                double x = Math.Pow(i+1,3);
                double y = Math.Pow(9,i%2) - 1;
                pointList.Add( new Point2( x, y ) ); // x^3,8 or 0: from Octave tests
            }
        //When
            Trajectory trj = new Trajectory( pointList );
            var arcL = trj.GetArcLength();

            var expArcL = new List<double>(){ 215.69, 1114.41, 2767.73 }; // Octave verified from input points
            int percentage = 1;
            List<double> precision = new List<double>();
            for(int i = 0; i < expArcL.Count; ++i)
            {
                precision.Add( expArcL[i] * percentage / 100 );
            }

        //Then
            bool allUpperBounded = true;
            bool allLowerBounded = true;
            for(int i = 0; i < precision.Count; ++i)
            {
                allUpperBounded &= arcL[i] <= expArcL[i] + precision[i];
                allLowerBounded &= arcL[i] >= expArcL[i] - precision[i];
            }
            Assert.True( allUpperBounded && allLowerBounded );
        }
        [Fact]
        public void TimeLawVisualTest()
        {
            Console.WriteLine("\nVISUAL TEST ONLY\n");
        //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );

            Console.WriteLine("\nTRAJECTORY\n");
            Console.WriteLine(trj);

            Console.WriteLine("\nNORMALIZED ARCLENGTH\n");
            trj.TimeLaw.ForEach( Console.WriteLine );
            
            Console.WriteLine("\nSr DOT\n");
            foreach(var tLaw in trj.TimeLaw) Console.WriteLine( tLaw.Derivate() );
            
            Console.WriteLine("\nTIME INTERVALS\n");
            trj.TimeIntervals.ForEach(Console.WriteLine);

            Console.WriteLine("\nINITIAL TIMES\n");
            trj.InitialTime.ForEach(Console.WriteLine);

            Console.WriteLine("\nPOINTS\n");
            trj.PointStream.ForEach(Console.WriteLine);

        //Then
            Assert.True(true); // Verified via [desmos](https://www.desmos.com/calculator). TODO: add graphic utility
        }

        [Theory]
        [InlineData(5.0,0)]
        [InlineData(10.0,1)]
        [InlineData(12.0,1)]
        [InlineData(17.0,2)]
        [InlineData(25.0,2)]
        public void iTrajectoryTest(double time, int supposedTrajectory)
        {
        //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );
        
        //Then
            Assert.Equal( supposedTrajectory, trj.iTrajectory(time) );
        }

        [Theory]
        [InlineData(0.0,0)]
        [InlineData(10.0,5)]
        [InlineData(25.0,15)]
        public void EvalTest(double time, int pointIndex)
        {
         //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );
        
        //Then
            Assert.Equal( pointList[pointIndex], trj.Eval(time) );
        }

        [Theory]
        [InlineData(0.0,0)]
        [InlineData(20.0,5)]
        [InlineData(50.0,15)]
        public void RescaleTrajectoryTest(double time, int pointIndex)
        {
         //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );
            Console.WriteLine($"Now the total time is {trj.TotalTime}");
            trj.RescaleTrajectory(50.0);
            Console.WriteLine($"Now the total time is {trj.TotalTime}");
        //Then
            Assert.Equal( pointList[pointIndex], trj.Eval(time) );
        }

        // this test is meant to show that no matter what, the first spline always end on the fifth point
        [Theory]
        [InlineData(2.0,5)]
        [InlineData(20.0,5)]
        [InlineData(50.0,5)]
        public void LinearRescaleSplineTest(double time, int pointIndex)
        {
         //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );
            trj.LinearRescaleSpline(0,time);

            Console.WriteLine("\nNORMALIZED ARCLENGTH\n");
            trj.TimeLaw.ForEach( Console.WriteLine );
            
            Console.WriteLine("\nTIME INTERVALS\n");
            trj.TimeIntervals.ForEach(Console.WriteLine);

            Console.WriteLine("\nINITIAL TIMES\n");
            trj.InitialTime.ForEach(Console.WriteLine);
        //Then
            Assert.Equal( pointList[pointIndex], trj.Eval(time) );
        }

         // this test is meant to show that no matter what, the first spline always end on the fifth point
        [Theory]
        [InlineData(2.0,5)]
        [InlineData(11.0,5)]
        [InlineData(14.0,5)]
        public void RescaleSplineTest(double time, int pointIndex)
        {
         //Given
            List<Point2> pointList = new List<Point2>();
            for(int iPoint = 0; iPoint < 16; ++iPoint)
            {   
                double x = iPoint;
                double y = 0;
                pointList.Add( new Point2( x, y ) );
            }

        //When
            var trj = new Trajectory( pointList );
            trj.RescaleSpline(0,time);

            Console.WriteLine("\nNORMALIZED ARCLENGTH\n");
            trj.TimeLaw.ForEach( Console.WriteLine );
            
            Console.WriteLine("\nTIME INTERVALS\n");
            trj.TimeIntervals.ForEach(Console.WriteLine);

            Console.WriteLine("\nINITIAL TIMES\n");
            trj.InitialTime.ForEach(Console.WriteLine);
        //Then
            Assert.Equal( pointList[pointIndex], trj.Eval(time) );
        }
    }
}
