using System;
using System.IO;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdTrajectories;
using Xunit;

namespace Wizard.Tests
{
    public class WizardTest
    {
        public MathWizard wiz = MathWizard.Wizard;
        [Fact]
        public void FindPointsTest()
        {
            var list = new List<Point2>()
            {
                new Point2(0,1),
                new Point2(0,1),
                new Point2(0,0),
                new Point2(0,0),
                new Point2(0,0),
                new Point2(0,0),
                new Point2(0,1),
                new Point2(0,0)
            };
            BoolDelegate<Point2> yEqualOne = (in Point2 valueP) => valueP.Y == 1;
            var result = MathWizard.Wizard.FindPoints<Point2>(list,yEqualOne);
            //Then
            bool isAllGood = true;
            isAllGood &= result.Count == 2;
            isAllGood &= result[0].pointList.Count == 2 && result[0].initialIndex == 0;
            isAllGood &= result[1].pointList.Count == 1 && result[1].initialIndex == 6;
            Assert.True(isAllGood);
        }

        [Fact]
        public void LocalizeOnTrajectoryTest()
        {
        //Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,   1),
                new Point2(2,  -1),
                new Point2(3,-0.5),
                new Point2(3, 0.5),
                new Point2(2,   1),
                new Point2(0,  -1)
            };
        Trajectory trj = new Trajectory(pointList);
        //When
        Point2 actualPoint = new Point2(4,0);
        double initialGuess = 0;

        var tOpt = wiz.LocalizeOnTrajectory(trj,actualPoint,initialGuess);
        //Then
        var ptDiff = trj.Eval(tOpt) - new Point2(2.5,0); //closest to the actualPoint from Desmos
        bool goodApprox = Math.Abs( ptDiff.AsVectorNorm() ) < Math.Pow(10,-3);
        Assert.True(goodApprox);
        }

        [Fact]
        public void BuildTrajectoryAfterError()
        {
        MathWizard wiz = MathWizard.Wizard;
            //Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,   1),
                new Point2(2,  -1),
                new Point2(3,-0.5),
                new Point2(3, 0.5),
                new Point2(2,   1),
                new Point2(0,  -1)
            };
            Trajectory trj = new Trajectory(pointList);
            //When
            Point2 actualPoint = new Point2(0.6,0, 5*Math.PI/4);
            double radius = 1.8;

            var newTrj = wiz.BuildTrajectoryAfterError(trj,actualPoint,radius);

            string fileTrj = @"../../../../../../tmp/trj.txt";
            string fileTrjNew = @"../../../../../../tmp/trjNew.txt";
            if(File.Exists(fileTrj)) File.Delete(fileTrj);
            if(File.Exists(fileTrjNew)) File.Delete(fileTrjNew);
            using (StreamWriter str = File.AppendText(fileTrj))
            {
                 string output = "";
                 trj.SplineStream[0].Polynomials[0].Coefs.ForEach((x)=> output += String.Format("{0:F2} ",x));
                 output += "\n";
                 trj.SplineStream[0].Polynomials[1].Coefs.ForEach((x)=> output += String.Format("{0:F2} ",x));
                 str.WriteLine(output);
            }

            using (StreamWriter str = File.AppendText(fileTrjNew))
            {
                for(int iSpl = 0; iSpl < newTrj.SplineStream.Count; ++iSpl)
                {
                    string output = "";
                    newTrj.SplineStream[iSpl].Polynomials[0].Coefs.ForEach((x)=> output += String.Format("{0:F2} ",x));
                    output += "\n";
                    newTrj.SplineStream[iSpl].Polynomials[1].Coefs.ForEach((x)=> output += String.Format("{0:F2} ",x));
                    str.WriteLine(output);
                }
            }
        }
    }
}
