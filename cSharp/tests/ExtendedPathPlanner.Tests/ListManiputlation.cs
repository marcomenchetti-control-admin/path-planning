using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdSplines;
using Ugv.PathPlanner.stdTrajectories;
using Xunit;

namespace Ugv.PathPlanner.Tests.stdTrajectories
{
    public class ListManiputlation
    {
        [Fact]
        public void InsertRangeTest()
        {
        // Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
            List<Point2> toInsertRange = new List<Point2>()
            {
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
        // When
            Trajectory trj = new Trajectory( pointList );
            trj.InsertRange(2,toInsertRange);
        // Then
            bool outEquality = true;
            for( int i = 0; i < expectedPointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == expectedPointList[i];
            }
            Assert.True(outEquality);
        }

        [Fact]
        public void GetRangeTest()
        {
        // Given
            List<Point2> toGetRange = new List<Point2>()
            {
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
        // When
            Trajectory trj = new Trajectory( expectedPointList );
            var gotFromTrj = trj.GetRange(2,5);
        // Then
            bool outEquality = true;
            for( int i = 0; i < gotFromTrj.Count; ++i )
            {
                outEquality &= gotFromTrj[i] == toGetRange[i];
            }
            Assert.True(outEquality);
        }

        [Fact]
        public void GetRangeLastTest()
        {
        // Given
            List<Point2> toGetRange = new List<Point2>()
            {
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
        // When
            Trajectory trj = new Trajectory( expectedPointList );
            var gotFromTrj = trj.GetRange(2,-1);
        // Then
            bool outEquality = true;
            for( int i = 0; i < gotFromTrj.Count; ++i )
            {
                outEquality &= gotFromTrj[i] == toGetRange[i];
            }
            Assert.True(outEquality);
        }

        [Fact]
        public void RemoveRangeTest()
        {
        // Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
            List<Point2> toRemoveRange = new List<Point2>()
            {
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
        // When
            Trajectory trj = new Trajectory( expectedPointList );
            trj.RemoveRange(2,5);
        // Then
            bool outEquality = true;
            for( int i = 0; i < pointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == pointList[i];
            }
            Assert.True(outEquality);
        }

        [Fact]
        public void RemoveRangeLastTest()
        {
        // Given
            List<Point2> pointList = new List<Point2>()
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4)
            };
            List<Point2> toGetRange = new List<Point2>()
            {
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
            List<Point2> expectedPointList = new List<Point2>() // Octave verified after correction
            {
                new Point2(0,0),
                new Point2(1,2),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(4,4),
                new Point2(2,4),
                new Point2(3,6),
                new Point2(4,8),
                new Point2(5,10)
            };
        // When
            Trajectory trj = new Trajectory( expectedPointList );
            trj.RemoveRange(6,-1);
        // Then
            bool outEquality = true;
            for( int i = 0; i < pointList.Count; ++i )
            {
                outEquality &= trj.PointStream[i] == pointList[i];
            }
            Assert.True(outEquality);
        }
    }
}
