# Unmanned Ground Vehicle Path Planner

This software allows you to create a trajectory from a stream of points, while updating and modifying the specified points.

Functionalities:

- [x] Build splines with continuity constraints.
- [x] Define a standard time law.
- [x] Rescale the timing of the whole trajectory.
- [x] Rescale a single time law, accomodating also the next one.
- [x] Rescale a single time law.