﻿using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using MathNet.Numerics.LinearAlgebra;

namespace Ugv.PathPlanner.stdSplines
{
    #region DEF
    /// <summary>Class for splines of sixth order, built using Beziér algorithm.</summary>
    /// This class contains the buit in functionalities to handle
    /// easily the implementation and evaluation of a Beziér spline of order 5.
    /// The order of the spline refers to the greatest power of the input parameter, and
    /// comes directly from the <see cref="m_numberOfCoefficients">number of coefficients</see>.
    ///
    ///  The class contains the following, added on top of its <see cref="BaseSpline">base class</see> :
    /// - public members:
    ///     - static <see cref="m_interpolationMatrix"></see>, taking care of the Beziér algorithm.
    ///     - static <see cref="m_numberOfCoefficients"></see>, fixing the number of entries in the polynomial representation.
    /// - public methods:
    ///     - <see cref="Interpolate"></see>, which adds an abstraction layer between the input stream of points
    /// and the polynomial representation of `x` and `y`.
    /// - protected methods:
    ///     - <see cref="PointsAssign"></see> which checks for errors while assigning a <c>List&lt;Point2&gt;</c>
    /// of points to the spline.
    /// 
    #endregion
    public class BezierSpline5 : BaseSpline
    {
        #region CONSTANTS
        /*--------------------- CONSTANTS ---------------------*/
        /// <summary>
        /// Matrix condensating the interpolation algorithm.
        /// </summary>
        public readonly static Matrix<double> m_interpolationMatrix = Matrix<double>.Build.DenseOfArray( new double[,]
            {
                { -1d,   5, -10,  10, -5, 1},
                {   5, -20,  30, -20,  5, 0},
                { -10,  30, -30,  10,  0, 0},
                {  10, -20,  10,   0,  0, 0},
                {  -5,   5,   0,   0,  0, 0},
                {   1,   0,   0,   0,  0, 0}
            }
        );

        /// <summary>
        /// number of coefficients for each spline, here 6
        /// </summary>
        public new readonly static int m_numberOfCoefficients = 6;
        #endregion

        #region PROPERTIES
        /*--------------------- PROPERTIES ---------------------*/
        #endregion

        #region CONSTRUCTORS
        /*--------------------- CONSTRUCTORS ---------------------*/
        /// <summary>
        /// Empty constructor
        /// </summary>
        /// <returns> <see cref="BaseSpline">base class</see> empty constructor. </returns>
        public BezierSpline5() : base(){}

        /// <summary>
        /// Standart constructor
        /// </summary>
        /// <param name="_points">Input points</param>
        /// <returns> <see cref="BaseSpline">base class</see> standard constructor if <see cref="PointsAssign"></see> is validated. </returns>
        public BezierSpline5(List<Point2> _points) : base(_points)
        {
            if(_points.Count != m_numberOfCoefficients) throw new InvalidOperationException("For this kind of splines the number of points is fixed to 6");
        }
        #endregion

        #region METHODS
        /*--------------------- METHODS ---------------------*/
        /// <returns> <code>new Polynomial[2] [x-polynomial, y-polynomial].</code> </returns>
        public override Polynomial[] Interpolate()
        {
            Polynomial[] polynomials = new Polynomial[2];
            Vector<double> xVector = Vector<double>.Build.Dense(6);
            Vector<double> yVector = Vector<double>.Build.Dense(6); // try Vector<double> yVector = xVector;
            for(int i = 0; i < m_numberOfCoefficients; ++i)
            {
                xVector[i] = Points[i].X;
                yVector[i] = Points[i].Y;
            }
            xVector = m_interpolationMatrix * xVector; // BAD reassign, watchagonnado?
            yVector = m_interpolationMatrix * yVector;

            polynomials[0] = new Polynomial( xVector.ToArray() );
            polynomials[1] = new Polynomial( yVector.ToArray() );
            return polynomials;
        }

        /// <param name="pointStream">a <c>List&lt;Point2&gt;</c> of points over which the spline will be built.</param>
        /// <returns> The same value as the input, if and only if the number of points is correct.</returns>
        protected override List<Point2> PointsAssign(List<Point2> pointStream)
        {
            if( pointStream.Count != m_numberOfCoefficients ) throw new InvalidOperationException("Wrong number of points!");
            else return pointStream;
        }

        //TODO: CorrectV
        //TODO: CorrectA
        #endregion

        #region OPERATORS
        /*--------------------- OPERATORS ---------------------*/
        #endregion
    }
}