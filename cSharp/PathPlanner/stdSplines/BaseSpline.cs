//#nullable enable
using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;

namespace Ugv.PathPlanner.stdSplines
{
    #region DEF
    /// <summary> Class defining the base properties for splines. </summary>
    /// 
    /// This class implements the standard set of members and methods that are needed in order to
    /// define, build and evaluate any kind of spline.
    /// 
    /// The class contains the following:
    /// - public members:
    ///     - <see cref="m_pointsLength"/>, defines the number of polynomials in <see cref="Polynomials"/> array.
    /// This is also linked to the dimension of the points over which the spline is interpolated.
    ///     - <see cref="m_numberOfCoefficients"/>, fixing the number of entries in the polynomial representation. Here
    /// it is useless but has to be overridden.
    /// - public properties:
    ///     - <see cref="Polynomials"/>, which is the array containing the polynomials describing the evolution of each 
    /// of the points, over which the spline is interpolated.
    ///     - <see cref="Points"/>, which contains the points, over which the spline is interpolated.
    /// - public methods:
    ///     - abstract <see cref="Interpolate"/>. This function builds the spline from the input points.
    ///     - abstract <see cref="PointsAssign"/>. This function is used to assign the input points to <see cref="Points"/>
    /// after a validity check.
    ///     - virtual <see cref="Eval"/> which evaluates the spline at a given value of the input parameter.
    ///     - virtual <see cref="ArcLength"/> which returns the length of the path described by the spline,
    /// computed via a numerical integration.
    /// - public operators: these methods implement the logical comparison between splines.
    /// 
    #endregion
    public abstract class BaseSpline : IEquatable< BaseSpline >
    {
        #region CONSTANTS
        /*--------------------- CONSTANTS ---------------------*/
        /// <summary>
        /// Number of components of the points defining the splines
        /// </summary>
        /// <value>Untill future development: 2</value>
        public readonly int m_pointsLength = 2; //TODO: General 

        /// <summary>
        /// List of points over which the spline is interpolated
        /// </summary>
        private List<Point2>/*?*/ m_points;

        /// <summary>
        /// Array of polynomials, one per component.
        /// </summary>
        private Polynomial[]/*?*/ m_polynomials;

        /// <summary>
        /// number of coefficients for each spline, here useless
        /// </summary>
        public readonly static int m_numberOfCoefficients = 0;
        #endregion

        #region PROPERTIES
        /*--------------------- PROPERTIES ---------------------*/
        /// <summary>
        /// Returns the array of polynomials, up to now only 2 polynomials ( planar points )
        /// </summary>
        public Polynomial[] Polynomials
        {
            get
            {
                return m_polynomials /*is not null ? polynomials : throw new NullReferenceException("Uninitialzed variable \"polynomials\"")*/;
            }
            private set
            {
                m_polynomials = value;
            }
        }

        /// <summary>
        /// Returns the List of m_points, equal in number to <see cref="m_numberOfCoefficients"/>
        /// </summary>
        public List<Point2> Points
        {
            get
            {
                return m_points /*is not null ? points : throw new NullReferenceException("Uninitialzed variable \"points\"")*/;
            }

            private set
            {
                m_points = PointsAssign(value);
                Polynomials = Interpolate();
            }
        }
        #endregion

        #region CONSTRUCTORS
        /*--------------------- CONSTRUCTORS ---------------------*/
        /// <summary>
        /// Empty constructor
        /// </summary>
        public BaseSpline()
        {
            this.m_points = null;
        }

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="_points">Input points</param>
        public BaseSpline(List<Point2> _points)
        {
            this.Points = _points;
        }
        #endregion

        #region METHODS
        /*--------------------- METHODS ---------------------*/
        /// <summary>
        /// Returns the coefficients of the polynomial describing the spline, given the input points
        /// </summary>
        /// <returns>The array of polynomials found by any given interpolation algorithm</returns>
        public abstract Polynomial[] Interpolate();

        /// <summary>
        /// Assign the input points to the List of points
        /// </summary>
        /// <param name="pointStream">Input list of points</param>
        /// <returns>The same input list only if validated</returns>
        protected abstract List<Point2> PointsAssign(List<Point2> pointStream);

        /// <summary>
        /// Evaluating the array of polynomials <see cref="Polynomials"/> at the given input
        /// </summary>
        /// <param name="input">Input parameter</param>
        /// <returns>The <c>Point2</c> made by evaluation of the polynomials</returns>
        public virtual Point2 Eval(double input)
        {
            /*if (Polynomials is not null)
            {*/
                return new Point2(Polynomials[0].Eval(input), Polynomials[1].Eval(input));
            /*}
            else throw new NullReferenceException("Polynomials is null here.");*/
        }

        /// <summary>
        /// Computes the length of the path defined by the spline, with a numeric integration
        /// </summary>
        /// <param name="precisionExponent"> An integration step </param>
        /// <returns>\f$ \int_{0}^{1} \sqrt{\dot{x}(s)^2 + \dot{y}(s)^2}ds \f$</returns>
        public virtual double ArcLength(int precisionExponent = -3)
        {
            double deltaS = Math.Pow(10, precisionExponent);
            double arcLength = 0;
            double s = 0;
            var vSquared = GetV2Poly();
            while (s < 1)
            {
                arcLength += Math.Sqrt(vSquared.Eval(s));
                s += deltaS;
            }
            return arcLength * deltaS;
        }

        /// <summary>
        /// this function Computes the polynomial associated to Vs to the second power.
        /// </summary>
        public Polynomial GetV2Poly( )
        {
            Polynomial vSquared = /*x_dot^2*/Polynomials[0].Derivate(1).Pow(2) + /*y_dot^2*/Polynomials[1].Derivate(1).Pow(2);
            return vSquared;
        }
        #endregion

        #region OPERATORS
        /*--------------------- OPERATORS ---------------------*/
        /// <summary>
        /// Compare two splines
        /// </summary>
        /// <param name="a">First spline</param>
        /// <param name="b">Second spline</param>
        /// <returns> a == b </returns>
        public static bool operator ==(BaseSpline a, BaseSpline b) => a.Equals(b);

        /// <summary>
        /// Compare two splines
        /// </summary>
        /// <param name="a">First spline</param>
        /// <param name="b">Second spline</param>
        /// <returns> a != b </returns>
        public static bool operator !=(BaseSpline a, BaseSpline b) => !a.Equals(b);
        #endregion

        #region OBJ_OVERLOAD
        /*--------------------- EQUALS & GETHASHCODE ---------------------*/
        /// <summary>
        /// Override ToString() function
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string outString = "X: " + Polynomials/*?*/[0].ToString();
            outString += "\nY: " + Polynomials/*?*/[1].ToString();
            return outString;
        }

        /// <summary>
        /// Override Equals
        /// </summary>
        /// <param name="obj">Input obj</param>
        /// <returns></returns>
        public bool Equals(BaseSpline/*?*/ obj)
        {
            bool outEqual = true;
            /*if (Polynomials is not null)
            {*/
                for (int i = 0; i < m_pointsLength; ++i)
                {
                    outEqual &= Polynomials[i].Equals(obj/*?*/.Polynomials[i]);
                }
            /*}
            else throw new NullReferenceException("Polynomials is null here.");*/

            if (!outEqual) return outEqual;

            for (int i = 0; i < Points.Count; ++i)
            {
                outEqual &= Points[i].Equals(obj/*?*/.Points[i]);
            }
            return outEqual;
        }

        /// <summary>
        /// Override Equals
        /// </summary>
        /// <param name="obj">Input obj</param>
        /// <returns></returns>
        public override bool Equals(object/*?*/ obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return (obj is BaseSpline) && Equals((BaseSpline)obj);
        }

        /// <summary>
        /// Override GetHashCode()
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            //TODO: override
            return base.GetHashCode();
        } 
        #endregion
    }
}