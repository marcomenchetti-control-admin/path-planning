﻿//#nullable enable
using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdSplines;
using Ugv.PathPlanner.stdObjects;
using MathNet.Numerics.LinearAlgebra;

namespace Ugv.PathPlanner.stdTrajectories
{
    #region DEF
    /// <summary>
    /// Class to represent a trajectory, from path to time law
    /// </summary>
    /// 
    /// This class implemets the methods and functionalities to handle trajectories.
    #endregion
    public class Trajectory
    {
        #region CONSTANTS
        /*--------------------- CONSTANTS ---------------------*/
        /// <summary>
        /// Private streamed list of points
        /// </summary>
        public List<Point2>/*?*/ m_streamPoints;

        /// <summary>
        /// Private list of points after correction
        /// </summary>
        public List<Point2>/*?*/ m_correctionPoints;

        /// <summary>
        /// Private list of generated splines
        /// </summary>
        public List<BezierSpline5>/*?*/ m_actualSplines;

        /// <summary>
        /// Private list of time law polynomials
        /// </summary>
        public List<Polynomial>/*?*/ m_timeLaw;

        /// <summary>
        /// Private list of time duration of a single spline
        /// </summary>
        public List<double>/*?*/ m_timeIntervals;

        /// <summary>
        /// Private list containing the sum of <see cref="m_timeIntervals"/>
        /// </summary>
        public List<double>/*?*/ m_initialTime;
        #endregion

        #region PROPERTIES
        /*--------------------- PROPERTIES ---------------------*/
        /// <summary>
        /// Point stream ACTUALLY used for spline interpolation
        /// </summary>
        /// <value></value>
        public List<Point2> PointStream
        {
            get
            {
                return /*m_streamPoints is not null ?*/ CompleteStream(m_streamPoints) /*: throw new NullReferenceException("Uninitialized points for trajectory!")*/;
            }
            private set
            {
                m_streamPoints = value;
                var totalStream = CompleteStream(value);
                PointCorrection = totalStream;
            }
        }

        /// <summary>
        /// Point after correction for continuity constraints
        /// </summary>
        /// <value></value>
        public List<Point2> PointCorrection
        {
            get
            {
                return m_correctionPoints /*is not null ? m_correctionPoints : throw new NullReferenceException("uninitialized m_correctionPoints!")*/;
            }
            private set
            {
                m_correctionPoints = CorrectStream(value);
                SplineStream = InterpolateFromStream(m_correctionPoints);
            }
        }

        /// <summary>
        /// List of splines used to represent the trajectory
        /// </summary>
        /// <value></value>
        public List<BezierSpline5> SplineStream
        {
            get
            {
                return m_actualSplines /*is not null ? m_actualSplines : throw new NullReferenceException("Uninitialized splines!")*/;
            }
            private set
            {
                m_actualSplines = value;
                (TimeLaw, TimeIntervals) = StandardTimeLaw(value);
            }
        }

        /// <summary>
        /// List of the polynomials used to represent the time law
        /// </summary>
        /// <value></value>
        public List<Polynomial> TimeLaw
        {
            get
            {
                return m_timeLaw /*is not null ? m_timeLaw : throw new NullReferenceException("uninitialized time law!")*/;
            }
            private set
            {
                m_timeLaw = value;
            }
        }

        /// <summary>
        /// List of time intervals, indicating the duration of each spline
        /// </summary>
        /// <value></value>
        public List<double> TimeIntervals
        {
            get
            {
                return m_timeIntervals /*is not null ? m_timeIntervals : throw new NullReferenceException("Uninitialized time intervlas")*/;
            }
            private set
            {
                m_timeIntervals = value;
                InitialTime = value;
            }
        }

        /// <summary>
        /// List of absolute times at the start of every spline.
        /// </summary>
        public List<double> InitialTime
        {
            get
            {
                return m_initialTime /*is not null ? m_initialTime : throw new NullReferenceException("m_initialTime member is not initialized")*/;
            }
            private set
            {
                double timeSum = 0;
                var tmpTimes = new List<double>();
                for (int i = 0; i < value.Count; ++i)
                {
                    tmpTimes.Add(timeSum);
                    timeSum += TimeIntervals[i];
                }
                m_initialTime = tmpTimes;
                TotalTime = timeSum;
            }
        }

        /// <summary>
        /// Sum of all the time intervals
        /// </summary>
        public double TotalTime
        {
            get;
            private set;
        }
        #endregion

        #region CONSTRUCTORS
        /*--------------------- CONSTRUCTORS ---------------------*/
        /// <summary>
        /// Standard constructor from a list of points
        /// </summary>
        /// <param name="pointStream">Input points</param>
        public Trajectory(List<Point2> pointStream)
        {
            if (pointStream.Count <= 1) throw new InvalidOperationException("Too few points!");
            PointStream = pointStream;
        }

        /// <summary>
        /// Standard constructor from a list of points + initial Position
        /// </summary>
        /// <param name="pointStream">Input points</param>
        public Trajectory(Point2 initialPoint, List<Point2> pointStream)
        {
            //if (initialPoint.Theta is null) throw new NullReferenceException("Initial point needs and initialized value for Theta");
            // reassign input variable
            var tmpStream = new List<Point2>(pointStream);

            // compute delta for intermediate point valutation
            var deltaPt = pointStream[0] - initialPoint;
            var deltaDist = deltaPt.AsVectorNorm() / 2; // add a "MID" point
            double initialAngle = initialPoint.Theta/*.Value*/; // Just save the initial angle

            // Instantiate intermediate X and Y as Initial + delta BUT thoward the initial angle direction
            var intermediateX = initialPoint.X + Math.Cos(initialAngle) * deltaDist;
            var intermediateY = initialPoint.Y + Math.Sin(initialAngle) * deltaDist;

            var intermediatePoint = new Point2(intermediateX, intermediateY); // assigned
            tmpStream.Insert(0, initialPoint);    // Put first the initial point
            tmpStream.Insert(1, intermediatePoint);  // Put second the intermediate point

            PointStream = tmpStream;
        }
        #endregion

        #region METHODS

        #region Initialization Methods
        /*--------------------- METHODS ---------------------*/
        /// <summary>
        /// Add to the stream the number of points necessary to complete the last spline
        /// </summary>
        /// <param name="stream"> input stream </param>
        /// <returns></returns>
        private List<Point2> CompleteStream(List<Point2> stream)
        {
            int residualPoints = (stream.Count - 1) % (BezierSpline5.m_numberOfCoefficients - 1);
            if (residualPoints == 0) return stream;

            int toAdd = BezierSpline5.m_numberOfCoefficients - residualPoints; // intervals not points

            var newStream = new List<Point2>();
            newStream.AddRange(stream);
            Point2 startFrom = newStream[newStream.Count - 2];
            Point2 deltaPt = newStream[newStream.Count - 1] - startFrom;

            for (int newInterval = 1; newInterval < toAdd; ++newInterval)
            {
                int currentCount = newStream.Count;
                Point2 iDeltaPt = newInterval * deltaPt / toAdd;

                // Add last to the back;
                newStream.Add(newStream[currentCount - 1]);
                ++currentCount;

                // Change last-1
                newStream[currentCount - 2] = startFrom + iDeltaPt;
            }
            return newStream;
        }

        /// <summary>
        /// Correct the stream, usually already completed, via velocity correction and acceleration correction.
        /// </summary>
        /// <param name="inputStream">Input stream of points</param>
        /// <returns></returns>
        private List<Point2> CorrectStream(List<Point2> inputStream)
        {
            int nSplines = (inputStream.Count - 1) / (BezierSpline5.m_numberOfCoefficients - 1);
            var newPts = new List<Point2>();
            newPts.AddRange(inputStream);

            for (int transitionIndex = 0; transitionIndex < nSplines - 1; ++transitionIndex)
            {
                int midPointInStream = (transitionIndex + 1) * (BezierSpline5.m_numberOfCoefficients - 1);

                // Velocity continuity
                Point2 velDN = inputStream[midPointInStream - 1];
                Point2 velUP = inputStream[midPointInStream + 1];
                Point2 velDiff = (velUP - velDN) / 2;

                newPts[midPointInStream + 1] = inputStream[midPointInStream] + velDiff;
                newPts[midPointInStream - 1] = inputStream[midPointInStream] - velDiff;

                // Acceleration continuity
                Point2 accDN = inputStream[midPointInStream - 2];
                Point2 accUP = inputStream[midPointInStream + 2];
                Point2 accMean = (accUP + accDN) / 2;

                Point2 newVDN = newPts[midPointInStream - 1];
                Point2 newVUP = newPts[midPointInStream + 1];

                newPts[midPointInStream + 2] = accMean + newVUP - newVDN;
                newPts[midPointInStream - 2] = accMean + newVDN - newVUP;
            }

            return newPts;
        }

        /// <summary>
        /// Computes the Beziér splines from the input stream of points
        /// </summary>
        /// <param name="stream">Input stream of points</param>
        /// <returns></returns>
        private List<BezierSpline5> InterpolateFromStream(List<Point2> stream)
        {
            int nSpline = (stream.Count - 1) / (BezierSpline5.m_numberOfCoefficients - 1);
            List<BezierSpline5> newSplineList = new List<BezierSpline5>();
            for (int iSpline = 0; iSpline < nSpline; ++iSpline)
            {
                int startIndex = iSpline * (BezierSpline5.m_numberOfCoefficients - 1);
                int streamCount = BezierSpline5.m_numberOfCoefficients;

                var singleSplineStream = stream.GetRange(startIndex, streamCount);

                BezierSpline5 newSpline = new BezierSpline5(singleSplineStream);
                newSplineList.Add(newSpline);
            }
            return newSplineList;
        }

        /// <summary>
        /// Computes the standard time law from the input splines
        /// </summary>
        /// <param name="inputSplines">Input splines</param>
        /// <returns></returns>
        private (List<Polynomial> outTimeLaw, List<double> outTimeIntervals) StandardTimeLaw(List<BezierSpline5> inputSplines)
        {
            List<double> arcLength = GetArcLength();
            List<Polynomial> sr = new List<Polynomial>(); // output arclength depending on time
            List<Polynomial> outTLaw = new List<Polynomial>(); //Useless, just for order, expressed in normalized arclength
            List<double> outTimes = new List<double>(); // output timings

            int nSpline = arcLength.Count;
            if (nSpline == 1)
            {
                var singleTime = arcLength[0];
                var coefs = new double[] { 2 / Math.Pow(singleTime, 2), 0 };
                var singleTLaw = new Polynomial(coefs).Integrate(0);
                return (new List<Polynomial>() { singleTLaw }, new List<double>() { singleTime });
            }
            for (int iSpline = 0; iSpline < nSpline; ++iSpline)
            {
                // below extremis (inVal and fnVal) are derived by imposing the gain (sr_dot) for equalizing Vr[i] and Vr[i+1]
                // knowing that:    -Vr[i] = V[i]/L[i]
                //                  - V[i] = sqrt( x_dot^2 + y_dot^2 ); 
                double inVal = iSpline == 0 ? 0 : (arcLength[iSpline] + arcLength[iSpline - 1]) / (2 * arcLength[iSpline - 1]); // (L[i] + L[i-1]) / (2L[i-1])
                double fnVal = iSpline == nSpline - 1 ? 0 : (arcLength[iSpline] + arcLength[iSpline + 1]) / (2 * arcLength[iSpline + 1]); // (L[i] + L[i+1]) / (2L[i+1])
                double dlVal = fnVal - inVal;

                // moving from extremis of gain to coefficients needed to impose those extremis and the integral of sr_dot == L[i] (arclength)
                outTimes.Add(/**/ (2 * arcLength[iSpline]) / (inVal + fnVal) /**/);

                // computing the polynomial
                sr.Add(/**/ new Polynomial(new double[] { dlVal / outTimes[iSpline], inVal }).Integrate(0) /**/); // Integral of the derivative of arclength (here linear)

                outTLaw.Add(sr[iSpline] / arcLength[iSpline]);
            }
            return (outTLaw, outTimes);
        }
        #endregion

        #region Utility and point manipulation
        /// <summary>
        /// Computes the arc length of every spline composing the trajectory
        /// </summary>
        /// <param name="precisionExponent">Integration step</param>
        /// <returns> Vector of arc lengths </returns>
        public List<double> GetArcLength(int precisionExponent = -3)
        {
            List<double> arcLengthV = new List<double>();
            foreach (var spline in SplineStream) arcLengthV.Add(spline.ArcLength(precisionExponent));
            return arcLengthV;
        }

        /// <summary>
        /// Return the unmanaged input stream
        /// </summary>
        /// <returns><c>m_streamPoints</c></returns>
        public List<Point2> GetRawStream()
        {
            return m_streamPoints;
        }

        /// <summary>
        /// Append to the stream a new list of points
        /// </summary>
        /// <param name="addedList">List of points to be appended</param>
        public void AddRange(List<Point2> addedList)
        {
            m_streamPoints/*?*/.AddRange(addedList);
            PointStream = m_streamPoints /*is not null ? m_streamPoints : throw new InvalidOperationException("Uninitialized initial points")*/;
        }

        /// <summary>
        /// Append to the stream a new point
        /// </summary>
        /// <param name="newPoint">Point to be appended</param>
        public void Add(Point2 newPoint)
        {
            m_streamPoints/*?*/.Add(newPoint);
            PointStream = m_streamPoints /*is not null ? m_streamPoints : throw new InvalidOperationException("Uninitialized initial points")*/;
        }

        /// <summary>
        /// Add the <c>midPointList</c> to the actual list of points
        /// </summary>
        /// <param name="initialIndex">starting index</param>
        /// <param name="midPointList">input list</param>
        public void InsertRange(in int initialIndex, in List<Point2> midPointList)
        {
            m_streamPoints.InsertRange(initialIndex,midPointList);
            PointStream = m_streamPoints;
        }

        /// <summary>
        /// Return the specified range of the input stream of points, if size is equal to -1 get from
        /// <c>initialIndex</c> to the last element
        /// </summary>
        /// <param name="initialIndex">Initial index</param>
        /// <param name="size">Size of the output list</param>
        /// <returns>The input list in the specified range</returns>
        public List<Point2> GetRange(in int initialIndex, in int size)
        {
            if( size >= 0 ) return m_streamPoints.GetRange(initialIndex,size);
            else
            {
                int newSize = m_streamPoints.Count - initialIndex;
                return m_streamPoints.GetRange(initialIndex,newSize);
            }
        }

        /// <summary>
        /// Remove the specified range of the input stream of points, if size is equal to -1 get from
        /// <c>initialIndex</c> to the last element
        /// </summary>
        /// <param name="initialIndex">Initial index</param>
        /// <param name="size">Size of the list to be removed</param>
        public void RemoveRange(in int initialIndex, in int size)
        {
            if( size >= 0 ) m_streamPoints.RemoveRange(initialIndex,size);
            else
            {
                int newSize = m_streamPoints.Count - initialIndex;
                m_streamPoints.RemoveRange(initialIndex,newSize);
            }
        }
        #endregion

        #region Time Valutations
        /// <summary>
        /// This function is used to identify which trjectory should be evalueated.
        /// </summary>
        /// <param name="time">Time instant</param>
        /// <returns>index <c>i</c> of the spline to be evaluated</returns>
        public int iTrajectory(double time)
        {
            for (int i = 0; i < m_actualSplines/*?*/.Count; ++i)
            {
                double lo = InitialTime[i];
                double hi = InitialTime[i] + TimeIntervals[i];
                if (time >= lo && time < hi) return i;
            }
            int endIndex = /*m_actualSplines is not null ?*/ m_actualSplines.Count - 1 /*: throw new NullReferenceException("m_actualSplines uninitialized")*/;
            if (time == InitialTime[endIndex] + TimeIntervals[endIndex]) return endIndex;
            throw new InvalidOperationException($"Time instant t={time}s is too large or too small");
        }

        /// <summary>
        /// Computes the normalized arclength with respect to the
        /// actual spline
        /// </summary>
        /// 
        /// This function may ends up being removed to achieve performances
        /// <param name="time">Actual time</param>
        /// 
        private (double s, int iSpl) GetS(double time)
        {
            int splineIndex = iTrajectory(time);
            double actual_s = TimeLaw[splineIndex].Eval(time - InitialTime[splineIndex]);
            return (actual_s, splineIndex);
        }

        /// <summary>
        /// This fuction evaluates the trajectory at a given time
        /// </summary>
        /// <param name="time">Time instant</param>
        /// <returns> The evaluation of the right spline, given the input value.</returns>
        public Point2 Eval(double time)
        {
            var (s, iSpl) = GetS(time);
            //Console.WriteLine($"Time:{time}, SplineIndex:{splineIndex}, TimeSum:{InitialTime[splineIndex]}");
            return SplineStream[iSpl].Eval(s);
        }

        /// <summary>
        /// This function return the velocity tangent to the path
        /// </summary>
        /// 
        /// <param name="s">Position on spline</param>
        /// <param name="iSpl">actual spline</param>
        /// <returns></returns>
        /// 
        public double GetV(double s, int iSpl)
        {
            var vSqrd_Poly = SplineStream[iSpl].GetV2Poly();

            double v_s = Math.Sqrt(vSqrd_Poly.Eval(s));

            return v_s;
        }

        /// <summary>
        /// This function computes the time derivative of the coarse angle 
        /// of the trajectory.
        /// </summary>
        /// 
        /// <param name="s">Actual position on the spline</param>
        /// <param name="iSpl">Actual spline</param>
        /// <returns>Angular velocity</returns>
        /// 
        public double GetOmega(double s, int iSpl)
        {
            // TODO: test
            double vel = GetV(s,iSpl);
            //if (vel > -Math.Pow(10, -4) && vel < Math.Pow(10, -4)) throw new DivideByZeroException("Close to singular value");
            // let the system handle this

            var x = SplineStream[iSpl].Polynomials[0];//.Compose(TimeLaw[iSpl]);
            var y = SplineStream[iSpl].Polynomials[1];//.Compose(TimeLaw[iSpl]);
            var xD1 = x.Derivate();
            var xD2 = xD1.Derivate();
            var yD1 = y.Derivate();
            var yD2 = yD1.Derivate();

            Polynomial num = yD2 * xD1 - yD1 * xD2; // this implements the numerator of the derivative of the coarse angle

            var numVal = num.Eval(s);
            var denVal = Math.Pow(vel, 2);
            return (numVal == 0 && denVal ==0)? 0 : numVal / denVal;
        }

        /// <summary>
        /// This funciton is used to return the curvature radius
        /// </summary>
        /// 
        /// <param name="time">Actual position on spline</param>
        /// <param name="iSpl">actual spline</param>
        /// 
        public double GetCR(double s, int iSpl)
        {
            return GetV(s,iSpl) / GetOmega(s,iSpl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public double GetCoarse(double time)
        {
            var spline = iTrajectory(time);
            var xD = SplineStream[spline].Polynomials[0].Derivate().Compose(TimeLaw[spline]); // derivative: dx( s(t) )/dt
            var yD = SplineStream[spline].Polynomials[1].Derivate().Compose(TimeLaw[spline]); // derivative: dy( s(t) )/dt
            return Math.Atan2(yD.Eval(time), xD.Eval(time));
        }
        #endregion

        #region Rescaling
        /// <summary>
        /// This funciton is used to rescale the whole trajectory up to the 
        /// specified input time.
        /// </summary>
        /// <param name="newTotalTime">New total time to be imposed</param>
        public void RescaleTrajectory(double newTotalTime)
        {
            int endIndex = TimeIntervals.Count - 1;
            double totalTime = InitialTime[endIndex] + TimeIntervals[endIndex];
            double rescalePercentage = newTotalTime / totalTime;

            var newTimeIntervals = new List<double>();
            for (int i = 0; i < endIndex + 1; ++i)
            {
                newTimeIntervals.Add(TimeIntervals[i] * rescalePercentage);
            }
            TimeIntervals = newTimeIntervals;

            for (int iPoly = 0; iPoly < TimeLaw.Count; ++iPoly)
            {
                TimeLaw[iPoly].IterativeRescale(1 / rescalePercentage);
            }
        }

        /// <summary>
        /// Build the interpolation matrix for <see cref="RescaleSpline"/>
        /// </summary>
        /// <param name="time">Input time</param>
        /// <returns>A matrix of double</returns>
        private Matrix<double> RescaleTimeInterpolationMatrix(double time)
        {
            double[,] entries =
            {
                { 0.0, 0.0, 1.0 },
                { Math.Pow(time,2), time, 1.0},
                { Math.Pow(time,3) / 3, Math.Pow(time,2) / 2, time}
            };
            return Matrix<double>.Build.DenseOfArray(entries);
        }

        /// <summary>
        /// This function rescale the single time law without affecting all the others.
        /// </summary>
        /// 
        /// <param name="iSpline"></param>
        /// <param name="newITime"></param>
        public void RescaleSpline(int iSpline, double newITime)
        {
            var iTimeLaw = new Polynomial(TimeLaw[iSpline].Derivate().Coefs);
            double vIn = iTimeLaw.Eval(0); // initial velocity
            double vFn = iTimeLaw.Eval(TimeIntervals[iSpline]);

            if (newITime > TimeIntervals[iSpline]) // this ensure that we are slowing down the time law -> parabola facing up
            {
                // The following considerations come from the study of the symbolic values of the coefficients
                // of the new **PARABOLIC** time law. We want a formulation without zeros -> negative delta
                // here's the final result.
                double coefA = Math.Pow(vIn, 2) + vIn * vFn + Math.Pow(vFn, 2);
                double coefB = -6 * (vIn + vFn);
                double coefC = 9.0;

                double dlt = Math.Pow(coefB, 2) - 4 * coefA * coefC;
                if (dlt < Math.Pow(10, -3) && dlt > -1 * Math.Pow(10, -3)) dlt = 0; // needed to discard numerical precision errors

                double solA = (-coefB + Math.Sqrt(dlt)) / (2 * coefA);
                double solB = (-coefB - Math.Sqrt(dlt)) / (2 * coefA);

                if (newITime > Math.Max(solA, solB))
                    // we can ignore the minimum between solA and solB because before the minimum, 
                    // the parabola face down, hence the timing can still decrease.
                    throw new InvalidOperationException($"Time is too long! Stay below {Math.Max(solA, solB)}s");
            }

            var conditions = Vector<double>.Build.DenseOfArray(new double[] { vIn, vFn, 1.0 });

            var newCoefs = RescaleTimeInterpolationMatrix(newITime).Solve(conditions);

            // compute the new spline
            TimeLaw[iSpline] = new Polynomial(newCoefs.ToArray()).Integrate(0);

            //update the time intervals
            var newTimes = new List<double>(TimeIntervals);
            newTimes[iSpline] = newITime;
            TimeIntervals = newTimes;
        }

        /// <summary>
        /// This function rescale the time of the specified spline, up to the specified time.
        /// </summary>
        /// 
        /// In detail, by accelerating (or decelerating) the first spline, the boundary condition for the next one change.
        /// Hence in this function, the following one is also modified so as to accomodate for the change.
        /// 
        /// To modify the single spline without changing anything else see: <see cref="RescaleSpline"/>.
        /// 
        /// <remarks>This is not implemented YET for the last time law</remarks>
        /// <param name="iSpline"></param>
        /// <param name="newITime"></param>
        /// 
        public void LinearRescaleSpline(int iSpline, double newITime)
        {

            if (iSpline == TimeIntervals.Count) throw new NotImplementedException($"Not yet defined for iSpline = {iSpline}");
            if (TimeIntervals.Count == 1)
            {
                RescaleTrajectory(newITime);
                return;
            }

            double velSplineIn = 0;
            double velSplineFn = 0;
            var newTimeIntervals = new List<double>(TimeIntervals);

            for (int splineIncrement = 0; splineIncrement < 2; ++splineIncrement)
            {
                if (splineIncrement == 0)
                {
                    // the first time law starts from the same value.
                    velSplineIn = TimeLaw[iSpline].Derivate().Eval(0);

                    // the final value is set so as to integrate to 1 after given newItime.
                    velSplineFn = (2 - velSplineIn * newITime) / newITime;
                }
                else
                {
                    // set the new initial value equal to the previously set final one.
                    velSplineIn = TimeLaw[iSpline].Derivate().Eval(newITime);

                    // the second time law end with the same value
                    velSplineFn = TimeLaw[iSpline + splineIncrement].Derivate().Eval(TimeIntervals[iSpline + splineIncrement]);
                }

                // select the finale time between the provided one: `newITime` and the one for the next time law
                // computed so as to integrate to 1 after fixing initial and final value with `velSplineIn` & `velSplineFn`
                var newTime = (splineIncrement == 0) ? newITime : 2 / (velSplineFn + velSplineIn);

                // change the time interval
                newTimeIntervals[iSpline + splineIncrement] = newTime;

                double[] tmpCoef = new double[] { velSplineFn - velSplineIn, velSplineIn };

                // update the time law
                var newTimeLaw = new Polynomial(tmpCoef);
                newTimeLaw.IterativeRescale(1 / newTime);
                TimeLaw[iSpline + splineIncrement] = newTimeLaw.Integrate(0);
            }

            //update the time interval
            TimeIntervals = newTimeIntervals;
            // TODO: rescale last
        }
        #endregion

        #endregion

        #region OBJ_OVERLOAD
        /// <summary>
        /// Override of ToString function
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string outString = "";
            for (int i = 0; i < SplineStream.Count; ++i)
            {
                outString += SplineStream[i].ToString();
                if (i < SplineStream.Count - 1) outString += "\n";
            }
            return outString;
        }
        #endregion
    }
}
