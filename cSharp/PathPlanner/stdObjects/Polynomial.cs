//#nullable enable
using System;
using System.Collections.Generic;

namespace Ugv.PathPlanner.stdObjects
{
    #region DEF
    /// <summary>
    /// Class for Polynomials.
    /// </summary>
    /// 
    /// This class is an implementation for polynomials, and it defines and implements the methods used to
    /// perform the standard operations among polynomials.
    /// 
    /// This class contains:
    /// - public members:
    ///     - <see cref="m_degree">degree</see>, which represent the highest power of the polynomial.
    ///     - <see cref="m_count">count</see>, which represent the number of coefficient in the polynomial. It is always computed
    /// as <see cref="m_degree">degree</see> + 1.
    /// - public properties:
    ///     - <see cref="Coefs"/>, which contains the coefficient of the polynomial.
    /// - public methods:
    ///     - <see cref="Eval"/>, which evaluates the polynomial at a given input.
    ///     - <see cref="Derivate"/>, which recursively computes the derivative of the polynomial. The order of derivation is
    /// specified by the input parameter.
    ///     - <see cref="Integrate"/>, which computes the integral of the polynomial, using the input parameter as the integration constant.
    ///     - <see cref="Pow"/>, which computes the power of the polynomial, whose order is specified by the input parameter.
    ///     - <see cref="DotProduct"/>, which multiplies coefficient by coefficient two polynomials
    /// <c>a</c> and <c>b</c>, mimicking the MATLAB <c> a.*b </c> product.
    ///     - <see cref="operator*(Polynomial,Polynomial)"/>, the only operator worth mentioning, which computes the convolution product
    /// between the two <see cref="Coefs">coefficient lists</see>, returning the actual product between two polynomials.
    /// - public operators and logical functionalities.
    /// 
    #endregion
    public class Polynomial : IEquatable<Polynomial>
    {
        #region CONSTANTS
        /*--------------------- CONSTANTS ---------------------*/
        /// <summary>
        /// Degree of the polynomial
        /// </summary>
        public readonly int m_degree;

        /// <summary>
        /// Number of coefficients
        /// </summary>
        public readonly int m_count;
        #endregion

        #region PROPERTIES
        /*--------------------- PROPERTIES ---------------------*/
        /// <summary>
        /// Coefficients of the polynomial
        /// </summary>
        /// 
        /// The element in index 0 is the one with the highest degree.
        /// <example>
        /// As an example, the polynomial \f$ p(x) = 3x^3 + 2x + 1\f$ will be represented by
        /// ~~~~~~{.cs}
        /// 
        ///     List&lt;double&gt; coefs = new List&lt;double&gt;() {3, 0, 2, 1};
        /// 
        ///     var poly = new Polynomial( coefs );
        /// 
        /// ~~~~~~
        /// </example>
        /// 
        public List<double> Coefs { get; private set; }
        #endregion

        #region CONSTRUCTORS
        /*--------------------- CONSTRUCTORS ---------------------*/
        /// <summary>
        /// Constructor from an already existing list of points
        /// </summary>
        /// <param name="coefs">Input coefficients</param>
        public Polynomial(List<double> coefs)
        {
            this.Coefs = coefs;
            this.m_degree = coefs.Count - 1;
            this.m_count = coefs.Count;
        }

        /// <summary>
        /// Constructor from an already existing array of points
        /// </summary>
        /// <param name="coefs">Input coefficients</param>
        public Polynomial(double[] coefs) : this(new List<double>(coefs)) { }

        /// <summary>
        /// Empty Constructor: build a constant 0
        /// </summary>
        public Polynomial() : this( new double[] {0} ) { }
        #endregion

        #region METHODS
        /*--------------------- METHODS ---------------------*/
        /// <summary>
        /// Evaluates the polynomial at a given input value
        /// </summary>
        /// <param name="x">Input value</param>
        /// <returns>\f$p(x) = \sum_{i=0}^{\mathrm{degree}}\mathrm{Coefs}[i]x^{\mathrm{degree}-i}\f$</returns>
        public double Eval(double x)
        {
            double outEval = 0;
            for (int coef_index = this.m_degree; coef_index >= 0; --coef_index)
            {
                outEval += Coefs[this.m_degree - coef_index] * Math.Pow(x, coef_index);
            }
            return outEval;
        }

        /// <summary>
        /// Computes the derivative of a polynomial.
        /// </summary>
        /// 
        /// <example>
        /// <para>Example:</para>
        /// The polynomial whose coefficients are [1 2 3] (\f$p(x) = x^2 + 2x +3\f$),
        /// has derivative with coefficients [2 2] (\f$p^{'}(x) = 2x + 2\f$)
        /// </example>
        /// <param name="order">Order of derivation</param>
        /// <returns>order-th derivative of the polynomial</returns>
        public Polynomial Derivate(int order = 1)
        {
            if (order < 0) throw new InvalidOperationException("No derivative exists for negative index!");
            else if (order == 0) return this;
            else if (order == 1)
            {
                List<double> new_coefs = new List<double>();
                for (int coefficient_index = this.m_degree; coefficient_index > 0; --coefficient_index)
                {
                    new_coefs.Add(coefficient_index * Coefs[this.m_degree - coefficient_index]);
                }
                return new Polynomial(new_coefs);
            }
            else return this.Derivate().Derivate(--order);
        }

        /// <summary>
        /// Returns the integral of a polynomial, plus the provided integration constant.
        /// </summary>
        /// 
        /// <example>
        /// Example:
        /// 
        /// The integral of the polynomial \f$p(x) = 3x^2 + 2x + 1\f$ is computed as
        /// ~~~~~~{.cs}
        ///     var coefs = new double[] {3 2 1};
        ///
        ///     var poly = new Polynomial( coefs );
        ///
        ///     var output = poly.Integrate(4);
        /// ~~~~~~
        /// which returns a polynomial <c>output</c>
        /// whose coefficients are <c>[1 1 1 4]</c> 
        /// representing the polynomial \f$P(x) = x^3 + x^2 + x + 4\f$
        /// </example>
        /// <param name="intergration_constant">Integration constant</param>
        /// <returns>integral of <c>this</c> + integration_constant</returns>
        /// 
        public Polynomial Integrate(double intergration_constant)
        {
            List<double> new_coefs = new List<double>();

            for (int coefficient_index = this.m_count; coefficient_index >= 0; --coefficient_index)
            {
                double new_val = coefficient_index > 0 ? Coefs[this.m_count - coefficient_index] / coefficient_index : intergration_constant;
                new_coefs.Add(new_val);

            }

            return new Polynomial(new_coefs);
        }

        /// <summary>
        /// Returns the string representation of the polynomial
        /// </summary>
        /// <returns>[ Coefs[0] ... Coefs[m_count-1] ]</returns>
        public override string ToString()
        {
            string output_string = "[";
            foreach (var coefficient in Coefs)
            {
                output_string += $" {coefficient}";
            }
            return output_string + " ]";
        }

        /// <summary>
        /// Computes the power of a polynomial.
        /// </summary>
        /// <example>
        /// As an example, the following code
        /// ~~~~~~{.cs}
        /// 
        ///     var coefs = new double[] { 1 1 };
        /// 
        ///     var poly = new Polynomial(){ coefs };
        /// 
        ///     var poly2 = poly.Pow(2);
        /// 
        ///     Console.WriteLine(poly2);
        /// ~~~~~~
        /// computes the second power of the polynomial \f$\mathrm{poly(x)} = x + 1\f$,
        /// which is \f$ \mathrm{poly2}(x) = x^2 + 2x + 1 \f$
        /// </example> 
        /// <param name="power">exponent of power elevation</param>
        /// <returns>The <c>power</c>-th elevation of <c>this</c> polynomial </returns>
        /// 
        public Polynomial Pow(int power)
        {
            if (power < 0) throw new InvalidOperationException("No power elevation for negative or zero indexes yet!");
            else if (power == 0) return new Polynomial( new double[] { 1});
            else if (power == 1) return this;
            else return this * this.Pow(--power);

        }

        /// <summary>
        /// Computes the convolution product between two different polynomials
        /// </summary>
        /// <param name="b">Second polynomial</param>
        /// <returns>sum for i from 0 to n of "a[i]*b[n-i]" </returns>
        private Polynomial Conv(Polynomial b)
        {
            //Console.WriteLine($"Conv({this.ToString()},{b.ToString()})");
            // Output
            List<double> new_coefs = new List<double>();

            // Convolution product
            int finalCount = this.m_count + b.m_count - 1;


            for (int new_coef_index = 0; new_coef_index < finalCount; ++new_coef_index)
            {
                new_coefs.Add(0);

                int iterationMin = Math.Max(0, new_coef_index - b.m_degree);
                int iterationMax = Math.Min(new_coef_index, this.m_degree) + 1;
                //Console.WriteLine($"\nNEW SUM from {iterationMin} to {iterationMax-1}");
                for (int iteration = iterationMin; iteration < iterationMax; ++iteration)
                {
                    int ib = new_coef_index - iteration;
                    //Console.WriteLine($"To {new_coef_index} ---> a[{iteration}] * b[{ib}]");
                    new_coefs[new_coef_index] += this.Coefs[iteration] * b.Coefs[ib];

                }
            }
            return new Polynomial(new_coefs);
        }

        /// <summary>
        /// Computes the product of two polynomials, coefficient by coefficient
        /// </summary>
        /// <param name="b">Second polynomial</param>
        /// <returns>C whose coefficients are: C[i] = a[i]*b[i]</returns>
        public Polynomial DotProduct(Polynomial b)
        {
            if (m_count != b.m_count) throw new InvalidOperationException("In DotProduct() argument can have different dimension from this");
            List<double> newCoefs = new List<double>();
            for (int i = 0; i < m_count; ++i)
            {
                newCoefs.Add(Coefs[i] * b.Coefs[i]);
            }
            return new Polynomial(newCoefs);
            //TODO: test
        }

        /// <summary>
        /// Return the array representation of the polynomial
        /// </summary>
        /// <returns>Array representation of List&lt;double&gt; Coefs</returns>
        public double[] ToArray()
        {
            var output = new double[m_count];
            for (int i = 0; i < m_count; ++i)
            {
                output[i] = Coefs[i];
            }
            return output;
        }

        /// <summary>
        /// This function is used to apply a **LINEAR** transformation
        /// to the input variable.
        /// </summary>
        /// 
        /// <example>
        /// As an example:
        /// ~~~{.cs}
        /// 
        ///     var coefs = new double[] { 1d, 1 };
        ///     
        ///     var poly = new Polynomial( coefs );
        /// 
        ///     poly.IterativeRescale( 2 );
        /// 
        /// ~~~
        /// returns a polynomial with coefficients <c>double[] { 2d, 1 }</c>.
        /// This polynomial has the same shape as the previous except that the abscissa is
        /// traveled 2 times faster.
        /// </example>
        /// <param name="gain"></param>
        public void IterativeRescale(double gain)
        {
            for (int iCoef = 0; iCoef < m_count; ++iCoef)
            {
                Coefs[iCoef] *= Math.Pow(gain, m_degree - iCoef);
            }
        }

        /// <summary>
        /// This function computes the composition of two polynomials
        /// </summary>
        /// 
        /// <example>
        /// As an example:
        /// ~~~{.cs}
        /// 
        ///     var aPoly = new Polynomial
        ///     (
        ///         new double[]{ 1, 2, 1 }
        ///     );
        ///     
        ///     var bPoly = new Polynomial
        ///     (
        ///         new double[]{ 1, 1}
        ///     );
        ///     
        ///     var cPoly = aPoly.Compose(bPoly);
        /// 
        /// ~~~
        /// this block of code returns a <c>Polynomial cPoly</c>
        /// whose coefficient are:
        /// ~~~{.cs}
        /// 
        ///     var cPolyCoefs = new double[]{ 1, 4, 4 }
        /// 
        /// ~~~
        /// which is the result of the resutl of:
        /// \f$ x^2 + 2x + 1 \f$ where \f$ x = y + 1 \f$
        /// </example>
        /// <param name="b">Input Poluninomial</param>
        /// <returns>The composition of polynomials</returns>
        public Polynomial Compose( Polynomial b )
        {
            var outPoly = new Polynomial();
            for( int iCoef = 0; iCoef < m_count; ++iCoef)
            {
                int iPow = m_degree - iCoef;
                outPoly += Coefs[iCoef] * b.Pow(iPow);
            }
            return outPoly;
        }
        #endregion

        #region OPERATORS
        /*--------------------- OPERATORS ---------------------*/
        /// <summary>
        /// Sum of two polynomials
        /// </summary>
        /// <param name="a">First polynomial</param>
        /// <param name="b">Second polynomial</param>
        /// <returns>a+b</returns>
        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            List<double> new_coefs = new List<double>();
            List<double> min_coefs = new List<double>();
            int min_count = 0;
            if (a.m_count >= b.m_count)
            {
                new_coefs = a.Coefs;
                min_coefs = b.Coefs;
                min_count = b.m_count;
            }
            else
            {
                new_coefs = b.Coefs;
                min_coefs = a.Coefs;
                min_count = a.m_count;
            }

            for (int index = 0; index < min_count; ++index)
            {

                new_coefs[new_coefs.Count - min_count + index] += min_coefs[index];

            }

            return new Polynomial(new_coefs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a">First polynomial</param>
        /// <returns> - a </returns>
        public static Polynomial operator -(Polynomial a)
        {
            List<double> new_coefs = new List<double>();
            foreach (var single_coef in a.Coefs)
            {
                new_coefs.Add(-single_coef);
            }
            return new Polynomial(new_coefs);
        }

        /// <summary>
        /// Return the difference between two polynomials
        /// </summary>
        /// <param name="a">First polynomial</param>
        /// <param name="b">Second polynomial</param>
        /// <returns>a - b</returns>
        public static Polynomial operator -(Polynomial a, Polynomial b) => (a + (-b));

        /// <summary>
        /// Returns the product of two polynomials (convolution product)
        /// </summary>
        /// <param name="a">Fist polynomial</param>
        /// <param name="b">Second polynomial</param>
        /// <returns> a.Conv( b ) </returns>
        public static Polynomial operator *(Polynomial a, Polynomial b)
        {

            return a.Conv(b);
        }

        /// <summary>
        /// Mutiplies the polynomial by a static gain
        /// </summary>
        /// <param name="a">Gain</param>
        /// <param name="polynomial">Polynomial</param>
        /// <returns> A polynomial whose coefficients has been multiplied by <c>a</c> </returns>
        public static Polynomial operator *(double a, Polynomial polynomial)
        {
            List<double> new_coefs = new List<double>();
            foreach (var actual_coef in polynomial.Coefs)
            {
                new_coefs.Add(a * actual_coef);
            }

            return new Polynomial(new_coefs);
        }

        /// <summary>
        /// Divides the polynomial by a static gain
        /// </summary>
        /// <param name="a">Gain</param>
        /// <param name="polynomial">Polynomial</param>
        /// <returns>A polynomial whose coefficients has been divided by <c>a</c></returns>
        public static Polynomial operator /(Polynomial polynomial, double a) => ((1 / a) * polynomial);

        /// <summary>
        /// Compare two polynomials
        /// </summary>
        /// <param name="a">First polynomial</param>
        /// <param name="b">Second polynomial</param>
        /// <returns> a == b </returns>
        public static bool operator ==(Polynomial a, Polynomial b) => a.Equals(b);

        /// <summary>
        /// Compare two polynomials
        /// </summary>
        /// <param name="a">First polynomial</param>
        /// <param name="b">Second polynomial</param>
        /// <returns> a != b </returns>
        public static bool operator !=(Polynomial a, Polynomial b) => !a.Equals(b); 
        #endregion

        #region OBJ_OVERLOAD
        /*--------------------- EQUALS & GETHASHCODE ---------------------*/
        /// <summary>
        /// Override of Equals(Polynomial)
        /// </summary>
        /// <param name="obj"></param>
        public bool Equals(Polynomial/*?*/ obj)
        {
            if (this.m_count != obj/*?*/.m_count) return false;

            bool output = true;
            for (int i = 0; i < this.m_count; ++i)
            {
                output &= (Coefs[i] == obj.Coefs[i]);
            }
            return output;
        }

        // override object.Equals
        /// <summary>
        /// Override of Equals(object)
        /// </summary>
        /// <param name="obj"></param>
        public override bool Equals(object/*?*/ obj)
        {
            return (obj is Polynomial) && this.Equals((Polynomial/*?*/)obj);
        }

        // override object.GetHashCode
        /// <summary>
        /// Override of GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return (m_degree, m_count, Coefs.ToString()).GetHashCode();
        }
        #endregion
    }
}
