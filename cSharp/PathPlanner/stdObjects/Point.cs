﻿//#nullable enable
using System;

namespace Ugv.PathPlanner.stdObjects
{
    #region DEF
    /// <summary>
    /// Class representing a 2D point.
    /// </summary>
    /// 
    /// This class defines a point in the **ICD** framework with all the identified properties.
    /// In addition to that, it implements the methods and locical operators to build algebraic
    /// operations among points.
    /// 
    /// This class contains:
    /// - public properties:
    ///     - <see cref="X"/>, the **x** component of the point.
    ///     - <see cref="Y"/>, the **y** component of the point.
    /// - public operators to implement the logical comparison among points.
    /// 
    /// Future developement:
    /// - public properties:
    ///     - <see cref="Theta"/> **TODO**
    ///     - <see cref="InitializationTime"/> **TODO**
    /// 
    #endregion
    public class Point2
    {
        #region PROPERTIES
        /*--------------------- PROPERTIES ---------------------*/

        /// <summary>
        /// X component of the cartesian representation of a point
        /// </summary>
        /// <value>[m]</value>
        public double X { get; private set; }

        /// <summary>
        /// Y component of the cartesian representation of a point
        /// </summary>
        /// <value>[m]</value>
        public double Y { get; private set; }

        /// <summary>
        /// Orientation of the body attached to a point expressed in radiants
        /// </summary>
        /// <value>[rad]</value>
        public double/*?*/ Theta { get; private set; }

        /// <summary>
        /// Time at which the point is generated
        /// </summary>
        /// <value></value>
        public DateTime/*?*/ InitializationTime { get; private set; }
        #endregion

        #region CONSTRUCTORS
        /*--------------------- CONSTRUCTORS ---------------------*/
        /// <summary>
        /// Full constructor
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">X coordinate</param>
        /// <param name="theta">Orientation angle</param>
        /// <param name="initializationTime">Time of initialization</param>
        private Point2(double x, double y, double/*?*/ theta, DateTime/*?*/ initializationTime)
        {
            this.X = x;
            this.Y = y;
            this.Theta = theta;
            this.InitializationTime = initializationTime;
        }

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        public Point2(double x, double y) : this(x, y, 0d, DateTime.FromOADate(0d))
        {
        }

        /// <summary>
        /// Standard constructor + theta
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="theta"> Heading value <param>
        public Point2(double x, double y, double theta) : this(x, y, theta, DateTime.FromOADate(0d))
        {
        }
        #endregion

        #region METHODS
        /*--------------------- METHODS ---------------------*/
        /// <summary>
        /// This function computes the string representation of a point
        /// </summary>
        /// <returns>[ X Y ]</returns>
        public override string ToString()
        {
            string output = "[ " + X.ToString() + " " + Y.ToString() + " ]";
            return output;
        }
        #endregion

        #region OPERATORS
        /*--------------------- OPERATORS ---------------------*/
        /// <summary>
        /// This function change sign to a point
        /// </summary>
        /// <param name="a">actual point</param>
        /// <returns>-a</returns>
        public static Point2 operator -(Point2 a)
        {
            return new Point2(-a.X, -a.Y, a/*?*/.Theta, DateTime.FromOADate(0d)/*null*/);
        }

        /// <summary>
        /// This function computes the sum of two points
        /// </summary>
        /// <param name="a">first point</param>
        /// <param name="b">second point</param>
        /// <returns>a + b</returns>
        public static Point2 operator +(Point2 a, Point2 b)
        {
            //bool yesThetaOp = a.Theta is not null && b.Theta is not null;
            return new Point2(a.X + b.X, a.Y + b.Y, 0d/*null*/, DateTime.FromOADate(0d)/*null*/);
        }

        /// <summary>
        /// This function computes the difference between two points
        /// </summary>
        /// <param name="a">first point</param>
        /// <param name="b">second point</param>
        /// <returns>a - b</returns>
        public static Point2 operator -(Point2 a, Point2 b) => a + (-b);

        /// <summary>
        /// This function multiplies a point by a scalar
        /// </summary>
        /// <param name="gain">scalar gain</param>
        /// <param name="a">actual point</param>
        /// <returns>gain * a</returns>
        public static Point2 operator *(double gain, Point2 a)
        {
            return new Point2(gain * a.X, gain * a.Y, 0d/*null*/, DateTime.FromOADate(0d));
        }

        /// <summary>
        /// This function multiplies a point by a scalar
        /// </summary>
        /// <param name="gain">scalar gain</param>
        /// <param name="a">actual point</param>
        /// <returns>gain * a</returns>
        public static Point2 operator *(Point2 a, double gain) => gain * a;

        /// <summary>
        /// This function divides a point by a scalar gain
        /// </summary>
        /// <param name="a">actual point</param>
        /// <param name="oneOverGain">scalar gain</param>
        /// <returns>a / oneOverGain</returns>
        public static Point2 operator /(Point2 a, double oneOverGain) => a * (1 / oneOverGain);

        /// <summary>
        /// This function computes a point whose entries 
        /// are the inverse of the actual points and are multiplied 
        /// by a scalar gain
        /// </summary>
        /// <param name="gain">scalar gain</param>
        /// <param name="a"> actual point</param>
        /// <returns>Point2( gain/a.X, gain/a.Y )</returns>
        public static Point2 operator /(double gain, Point2 a) => new Point2(gain / a.X, gain / a.Y);

        /// <summary>
        /// This function computes the equality between two points
        /// </summary>
        /// <param name="a">first point</param>
        /// <param name="b">second point</param>
        /// <returns>a == b</returns>
        public static bool operator ==(Point2 a, Point2 b) => a.Equals(b);

        /// <summary>
        /// This function computes the negated value of the equality between two points
        /// </summary>
        /// <param name="a">first point</param>
        /// <param name="b">second point</param>
        /// <returns> a != b </returns>
        public static bool operator !=(Point2 a, Point2 b) => !a.Equals(b); 
        #endregion

        #region OBJ_OVERLOAD
        /*--------------------- EQUALS & GETHASHCODE ---------------------*/
        /// <summary>
        /// Equals a generic object 
        /// </summary>
        /// <param name="obj">Comparison term</param>
        public override bool Equals(object/*?*/ obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return this.GetHashCode() == obj.GetHashCode();
        }

        /// <summary>
        /// Computes the hash code of the touple 
        /// <code>{X, Y}</code>
        /// </summary>
        public override int GetHashCode()
        {
            return new { X, Y }.GetHashCode();
        }

        /// <summary>
        /// Computes the norm as if it were a vector starting from the origin
        /// </summary>
        /// <returns></returns>
        public virtual double AsVectorNorm()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        /// <summary>
        /// Compute the angle of the vector representation of the point
        /// </summary>
        /// <returns></returns>
        public virtual double AsVectorAtan2()
        {
            return Math.Atan2(Y, X);
        }
        #endregion
    }
}
