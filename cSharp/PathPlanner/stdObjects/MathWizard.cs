using System;
using System.Collections.Generic;
using Ugv.PathPlanner.stdObjects;
using Ugv.PathPlanner.stdSplines;
using Ugv.PathPlanner.stdTrajectories;
// Supposed Generic Implementation
//using MathNet.Numerics.LinearAlgebra;

namespace Ugv.PathPlanner.stdObjects
{
// Supposed Generic Implementation
//    public delegate TypeOut MathDelegate<TypeOut,TypeIn>(TypeIn inValue);
    /// <summary>
    /// Function type for standard SISO operations
    /// </summary>
    /// <param name="inValue">Input parameter</param>
    /// <returns></returns>
    public delegate double MathDelegate(double inValue);

    /// <summary>
    /// Function type for standard in SISO operations
    /// </summary>
    /// <param name="inValue">Input parameter</param>
    /// <returns></returns>
    public delegate bool BoolDelegate<T>(in T inValue);

    /// <summary>
    /// This *Math Wizard* implements the necessary algorithms
    /// for a flexible use of the path planner
    /// </summary>
    public sealed class MathWizard
    {
        private static MathWizard wizard = null;

        /// <summary>
        /// Public insantiation of the <c>MathWizard Singleton</c>
        /// </summary>
        public static MathWizard Wizard
        {
            get
            {
                if( wizard == null ) wizard = new MathWizard();
                return wizard;    
            }
        }

// Supposed Generic Implementation
//        public static TypeIn GNMinimization<TypeOut,TypeIn>
//        ( 
//            in MathDelegate<TypeOut,TypeIn> toMinimize,
//            in TypeIn initialGuess
//        ) 
//                where TypeOut : IComparable
//                where TypeIn : new()
        /// <summary>
        /// This function implements the Newton-Gauss Algorithm for minimization of an ERROR function.
        /// </summary>
        /// 
        /// Finds the zero of the input function
        /// <param name="errorFunction">Delegate function to be minimized</param>
        /// <param name="initialGuess">Input parameter, suitable for local research</param>
        /// <param name="exponent">Exponent of the precision order of magnitude to validate the minimum</param>
        /// <returns>The input for wich the <c>errorFunction</c> return value is 0</returns>
        public double GNInversion( in MathDelegate errorFunction, in double initialGuess, in int exponent = -3)
        {
            int maxIteration = 1000; // Now fixed
            int gaussTryMax = 5; // Now Fixed
            int newtTryMax = 5; // Now Fixed
            bool gaussStuck = false; // Check if it is usefull to continue
            bool newtStuck = false; // Check if it is usefull to continue
            bool jDetSmall = false; // // Check if it is usefull to continue with newt

            // precision (fixed) over which the representation is considered 
            // singular and newton switches with gauss
            double determinantSingularity = Math.Pow(10,-3);

            double precision = Math.Pow(10,exponent); // errorFunction must be below this value to exit the loop, or reach max iteration
            double step = Math.Pow(10,-4); // increment (fixed) to compute the numerical derivative of a function

            // algorithm start from here
            double q0 = initialGuess;
            double q = q0;
            for(int iteration = 0; iteration < maxIteration; ++iteration)
            {  

                #region Gauss Newton Iterations

                // computes the values used to perform both algorithms
                var jacobian = ( errorFunction(q+step) - errorFunction(q-step) ) / (2 * step);
                var error = errorFunction(q);

                // Singularity check: Is the jacobian determinant too small?
                //  If TRUE: perform Gauss
                //  If FALSE: perform Newton
                jDetSmall = Math.Abs(jacobian) < determinantSingularity;
                if(jDetSmall) newtStuck = true;
                Console.WriteLine($"jDetSmall: {jDetSmall}, gaussStuck: {gaussStuck}, newtStuck: {newtStuck}");
                if(!gaussStuck)
                {
                    // Gauss algorithm: JUST ASSIGN TO `q`
                    #region GAUSS

                    double alpha = 1;

                    for (int gaussTry = gaussTryMax; gaussTry > 0; --gaussTry)
                    {

                        // at each iteration makes newAlpha smaller
                        double newAlpha = alpha * gaussTry / gaussTryMax;

                        // store the new try for gauss algorithm optimum
                        double qGaussTry = q - jacobian * error * newAlpha;

                        // New error good: can exit
                        if(/**/ Math.Abs(error) > Math.Abs( errorFunction( qGaussTry ) )/**/)
                        {
                            q = qGaussTry;
                            newtStuck = false;
                            break;
                        } else if( gaussTry == 1 ) gaussStuck = true; //If error no good and last iteration: WE STUCK

                    }

                    #endregion

                } else if(!jDetSmall && !newtStuck)
                {
                    // Newton algorithm: JUST ASSIGN TO `q`
                    #region NEWTON
                    
                    double alpha = 1;

                    for (int newtTry = newtTryMax; newtTry > 0; --newtTry)
                    {

                        // at each iteration makes newAlpha smaller
                        double newAlpha = alpha * newtTry / newtTryMax;

                        // store the new try for gauss algorithm optimum
                        double qNewtTry = q - error * newAlpha / jacobian ;

                        // New error good: can exit
                        if(/**/ Math.Abs(error) > Math.Abs( errorFunction( qNewtTry ) )/**/)
                        {
                            q = qNewtTry;
                            gaussStuck = false;
                            break;
                        } else if( newtTry == 1 ) newtStuck = true; //If error no good and last iteration: WE STUCK

                    }
                    // next iteration could get gauss out of singularity 
                    // even if Newton can't be reached anymore if gaussStuck became true.
                    // Added for possible future implementations
                    
                    #endregion
                } else
                {
                    throw new InvalidProgramException($"Algorithm is stuck at q0: {q0}. No zero error solution. Try a new initial guess");
                }

                #endregion

                // ! REMEMBER ! the minimum for an error function should be zero
                #region ERROR_CHECK

                // The additional condition of gaussStuck has been added because because if that is stuck
                // nothing is reachable anymore. For future development this has to be modified
                if( Math.Abs(errorFunction(q)) < precision || (gaussStuck && newtStuck)) 
                {
                    break;
                }

                #endregion

            }
            q0=q;
            //Console.WriteLine($"jDetSmall: {jDetSmall}, gaussStuck: {gaussStuck}, newtStuck: {newtStuck}");
            //check if anything has been updated    
            return q0;
        }

        /// <summary>
        /// Find any point in the list satisfying the <c>evalFunction</c> constraint
        /// </summary>
        /// 
        /// This method finds any point in the input list satisfying the <c>evalFunction</c> constraint
        /// and gropus them by adjacency of index. 
        /// 
        /// Then returns a <c>List</c> of couples where:
        ///     - the <c>pointList</c> element is a List of adjacent points satisfying <c>evalFunction</c>.
        ///     - the <c>initialIndex</c> element is an index. It localize the first element of <c>pointList</c>
        ///         on the the input element <c>inputList</c>
        /// <param name="inputList">Input Parameter</param>
        /// <param name="evalFunction">Evaluation criteria</param>
        /// <returns></returns>
        public List< (int initialIndex, List<T> pointList) > FindPoints<T>(in List<T> inputList, BoolDelegate<T> evalFunction)
        {
            var outputList = new List< (int, List<T>) >();

            bool previousGood = false;
            bool actualGood = false;
            bool lastElement = false;

            int startIndex = 0;
            int size = 0;

            for( int actualIndex = 0; actualIndex < inputList.Count; ++actualIndex )
            {
                // update of boolean inputs
                previousGood = actualGood;
                actualGood = evalFunction( inputList[ actualIndex ] );
                lastElement = actualIndex == inputList.Count - 1;

                // actuation via Karnaugh maps
                //
                //  1: update the starting index of the next input element
                //
                //  2: update the size of the new input element
                //
                //  3: assign to outIndex
                //      sideEffect: -reset size
                //

                /* 1 */ if( actualGood && !previousGood ) startIndex = actualIndex;
                /* 2 */ if( actualGood ) ++size;
                /* 3 */ if( (actualGood && lastElement) || (previousGood && !actualGood) )
                        {
                            outputList.Add(/**/(startIndex, inputList.GetRange(startIndex,size))/**/);
                            size = 0;
                        }

            }
            return outputList;
        }
        
        /// <summary>
        /// Find the time minimizing the distance between the reference point <c>actualPoint</c>
        /// and the <c>trajectory</c>
        /// </summary>
        /// <param name="trajectory">Input Trajectory</param>
        /// <param name="actualPoint">Point with respect to which the localization is performed</param>
        /// <param name="initialGuess">Initial guess fot the localization</param>
        /// <returns></returns>
        public double LocalizeOnTrajectory(Trajectory trajectory, Point2 actualPoint, double initialGuess)
        {
            MathDelegate distanceFunction = (double x) => (actualPoint - trajectory.Eval(Math.Max(0,Math.Min(x,trajectory.TotalTime)))).AsVectorNorm();
            double tOpt = GNInversion(distanceFunction, initialGuess);
            return tOpt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trajectory"></param>
        /// <param name="actualPoint"></param>
        /// <param name="actualTime"></param>
        /// <param name="radius_EFF"></param>
        public Trajectory BuildTrajectoryAfterError(Trajectory trajectory, Point2 actualPoint, double radius_EFF, double actualTime = 0 )
        {

#region 1: Instatiate the necessary variables
            // Point2 on the spline
            var timeOnSpline = LocalizeOnTrajectory(trajectory,actualPoint,actualTime);
            var pointOnSpline = trajectory.Eval(timeOnSpline);

            // distance from the actual point (AS FUNCTION)
            BoolDelegate<Point2> TooCloseToActualPoint = (in Point2 x) => (x - actualPoint).AsVectorNorm() <= radius_EFF;

            // Find the points of the trajectory which are too close to the actual point
            var oldPoints = trajectory.GetRawStream();
            var pointsTooClose = FindPoints<Point2>( oldPoints, TooCloseToActualPoint );

            // Instantiate the new points that will replace the oldPoints
            var newPoints = new List<Point2>();
#endregion


            if(pointsTooClose.Count != 0)
            {
#region 2: If there are points too close do the trick
    #region 2.1: Find which block is closest to pointOnSpline
                int minBlockIndex = 0;
                double minDistance = radius_EFF;
                for(int iBlock = 0; iBlock < pointsTooClose.Count; ++iBlock)
                {
                    Action<Point2> minUpdate = (Point2 inputPt) => 
                    {
                        var actualDist = (inputPt - pointOnSpline).AsVectorNorm();
                        if( Math.Abs(actualDist) < Math.Abs(minDistance) )
                        {
                            minDistance = actualDist;
                            minBlockIndex = iBlock;
                        }
                    };
                    pointsTooClose[iBlock].pointList.ForEach( minUpdate );
                }
    #endregion

    #region 2.2: Computes the vectors necessary to the turning direction decision
                // endIndex represent the index of the last point to be eliminated
                int endIndex = pointsTooClose[minBlockIndex].initialIndex + pointsTooClose[minBlockIndex].pointList.Count - 1;

                // Position Vector
                var positionVector = actualPoint - pointOnSpline;
                positionVector /= positionVector.AsVectorNorm();
                var positionAngle = positionVector.AsVectorAtan2();

                // Pointing vector
                var pointingAngle  = Btwn_PI(actualPoint.Theta);
                var pointingVector = new Point2( Math.Cos( pointingAngle ), Math.Sin( pointingAngle ) );
                pointingVector /= pointingVector.AsVectorNorm();

                // First good point vector
                var firstGoodPointVector = oldPoints[endIndex + 1] - pointOnSpline;
                firstGoodPointVector /= firstGoodPointVector.AsVectorNorm();
                var firstGoodPointAngle = firstGoodPointVector.AsVectorAtan2();
    #endregion
            
                //   2.3:  Find the turning direction (sign of the Z component of the vector product),
                //           in this case (since the vectors are planar) it is AxB = [0; 0; AxBy - AyBx]:
                //       - Vector A will be the distance UNIT VECTOR from the actual point on the trajectory
                //           and the position of the Drone.
                //       - Vector B will be given by the direction the Drone is facing
                //       If the turning direction is zero, they are parallel, hence use:
                //           - Vector A As Vector B
                //           - Vector B As the distance UNIT VECTOR from the actual point on the trajectory
                //               and the first point that has not been deleted.
                //       If also those are parallel, simply go ahead.
                //
                // /////////////////////////////////////////////////////////// //
                //       
                //  NOTE:       The choice of using also the position vector in the decision process
                //          is due to the fact that we want FIRST to be headed toword the old trajectory
                //          and only in a SECOND place to move onto the next point.
                //              Neglecting this kind of reasoning the whole process could be heavily 
                //          simplified.
                //
                // /////////////////////////////////////////////////////////// //
                //       DONE WITH KARNAUGH MAPS.
                //       The input states are:
                //           - Accw: TRUE = counter clock wise, FALSE = clock wise IFF A0 = FALSE    (On FIRST condition)
                //           - A0:   TRUE = product is zero -> no turning, FALSE = product not zero  (On FIRST condition)
                //       The output states are:
                //           - Ccw:  TRUE = counter clock wise, FALSE = clock wise IFF C0 = FALSE
                //           - Ahead:   TRUE = go ahead FALSE = follow Ccw
                bool almostAhead = Math.Abs( Btwn_PI(firstGoodPointAngle - pointingAngle) ) < Math.Abs( Math.PI ) / 2;
                if( !almostAhead )
                {
    #region 2.3: Find turning direction

        #region 2.3.1: Compute the cross products
                    // temp function to compute the crossproduct component along Z axis
                    Func<Point2,Point2,double> crossProdZ = (Point2 A, Point2 B) => A.X * B.Y - A.Y * B.X;

                    var AxB = crossProdZ( positionVector, pointingVector );
                    var AxB_secondChoice = crossProdZ( pointingVector, firstGoodPointVector );
        #endregion

        #region 2.3.2: Computes the Karnaugh input/output
                    // input
                    bool A0, B0, Accw, Bccw, Ahead, Ccw;
                    A0 = AxB == 0;
                    B0 = AxB_secondChoice == 0;
                    Accw = AxB > 0;
                    Bccw = AxB_secondChoice > 0;

                    //output
                    Ahead = A0 && B0; // Result from Karnaugh maps
                    Ccw = (A0 && Bccw && !B0) || (Accw && !A0) ; // Result from Karnaugh maps
        #endregion
    #endregion
    #region 2.4: Add the via points if required
                    if(Ahead)
                    {
                        // Do nothing! just take the FIRST good point.
                    }
                    else
                    {
        #region 2.4.1: Make firstGoodPointAngle conformant to the turning direction
                        // If Ccw -> firstGoodPointAngle should be greater than pointing angle
                        // If Cw -> firstGoodPointAngle should be smaller than pointing angle
                        double deltaAngle;
                        if(Ccw && firstGoodPointAngle < pointingAngle) deltaAngle = Btwn_2PI(firstGoodPointAngle - pointingAngle);
                        else if(!Ccw && firstGoodPointAngle > pointingAngle) deltaAngle = Btwn_m2PI(firstGoodPointAngle - pointingAngle);
                        else deltaAngle = firstGoodPointAngle - pointingAngle;
        #endregion

        #region 2.4.2: Add new points.
                        // this value is tuned so that it enforce the trajectory
                        // to pass on one of these new points.
                        int totNewPoints = 5;
                        var radius = Math.Sqrt(2) * radius_EFF;
                        for(int newPointIndex = 1; newPointIndex < totNewPoints; ++newPointIndex)
                        {
                            var tmp_angle = pointingAngle + deltaAngle * (newPointIndex)/(totNewPoints);
                            var tmp_newPoint = new Point2( Math.Cos(tmp_angle), Math.Sin(tmp_angle) ) * radius + actualPoint;
                            newPoints.Add(tmp_newPoint);
                        }
        #endregion
                    }
    #endregion
                }
                oldPoints.RemoveRange(0,endIndex + 1); // second argument is a count
#endregion
            }
            newPoints.AddRange(oldPoints);
            return new Trajectory(actualPoint,newPoints);
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public double Btwn_PI(double angle)
        {
            if( angle < -Math.PI )
            {
                var tmp_angle = angle + 2 * Math.PI;
                return Btwn_PI(tmp_angle);
            }
            else if( angle > -Math.PI && angle < Math.PI )
            {
                return angle;
            }
            else if( angle > Math.PI)
            {
                var tmp_angle = angle - 2 * Math.PI;
                return Btwn_PI(tmp_angle);
            }
            else return angle; // angle equal to PI
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public double Btwn_2PI(double angle)
        {
            return Btwn_PI(angle - Math.PI) + Math.PI;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public double Btwn_m2PI(double angle)
        {
            return -Btwn_2PI(-angle);
        }
    }
}